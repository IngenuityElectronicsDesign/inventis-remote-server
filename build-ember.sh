echo 'Updating NPM Modules for Sails'
npm install
echo 'Finished Updating NPM Modules for Sails'
cd inventis
echo 'Updating NPM Modules for Ember'
npm install
echo 'Finished Updating NPM Modules for Ember'
echo 'Updating bower packages for Ember'
bower install
echo 'Finished updating bower packages for Ember'
ember build --environment=development
cd ..
rm views/index.ejs
rm -r assets/assets/
cp -R inventis/dist/assets/ assets/assets/
cp inventis/dist/index.html views/index.ejs
echo 'Finished building and copying files!'
