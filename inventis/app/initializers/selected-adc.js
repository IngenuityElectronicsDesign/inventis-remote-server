export default {
	name: 'selected-adc',
	initialize: function(container, application) {
		var SelectedADC = Ember.Object.extend({adc: null});
		application.register('selected-adc:main', SelectedADC);
		application.inject('controller', 'selected-adc', 'selected-adc:main');
		application.inject('route', 'selected-adc', 'selected-adc:main');
	}
};