export default {
	name: 'selected-tab',
	initialize: function(container, application) {
		var SelectedTab = Ember.Object.extend({
			tab: ''
		});
		application.register('selected-tab:main', SelectedTab);
		application.inject('controller', 'selected-tab', 'selected-tab:main');
		application.inject('route', 'selected-tab', 'selected-tab:main');
	}
};