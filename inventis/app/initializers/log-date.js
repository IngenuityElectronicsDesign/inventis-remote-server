export default {
	name: 'log-start-date',
	initialize: function(container, application) {
		var LogDate = Ember.Object.extend({
			startDate: new Date(),
			endDate: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000)
		});
		
		application.register('log-date:main', LogDate);
		application.inject('controller', 'log-date', 'log-date:main');
		application.inject('route', 'log-date', 'log-date:main');
	}
};