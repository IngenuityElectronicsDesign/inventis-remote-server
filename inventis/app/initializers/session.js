export default {
	name: 'session',
	initialize: function(container, application) {
		application.deferReadiness();
		$.getJSON('/user/isLoggedIn')
		.then(function(data) {
			var Session;
			if(data.isAuthenticated) {
				Session = Ember.Object.extend({userId: data.userId});
			} else {
				Session = Ember.Object.extend({userId: -1});
			}
			application.register('session:main', Session);
			application.inject('controller', 'session', 'session:main');
			application.inject('route', 'session', 'session:main');
			application.advanceReadiness();
		});
	}
};