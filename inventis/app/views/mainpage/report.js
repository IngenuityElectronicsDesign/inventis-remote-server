export default Ember.View.extend({
	didInsertElement: function() {
		var startTimePicker = this.$("#startdatetimepicker");
		startTimePicker.datetimepicker({
			defaultDate: new Date()
		});
		startTimePicker.on('dp.show', (function() {
			this.get('controller').send('startDateShown');
		}).bind(this));

		startTimePicker.on('dp.change', (function() {
			this.get('controller').send('startDateChanged');
		}).bind(this));

		var endTimePicker = this.$("#enddatetimepicker");
		endTimePicker.datetimepicker({
			defaultDate: new Date()
		});
		endTimePicker.on('dp.show', (function() {
			this.get('controller').send('endDateShown');
		}).bind(this));

		endTimePicker.on('dp.change', (function() {
			this.get('controller').send('endDateChanged');
		}).bind(this));
	}	
});