export default Ember.View.extend({
	templateName: 'device-list-row',
	location: {},
	click: function() {
		this.get('controller').send('rowClicked', this.location);
	}
});