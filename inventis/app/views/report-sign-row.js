export default Ember.View.extend({
	tagName: 'a',
	classNameBindings: ['mainClass', 'selected:active'],
	mainClass: 'list-group-item',
	selected: false,
	defaultTemplate: Ember.Handlebars.compile('Router: {{sign.routerId}}'),
	sign: null,
	//templateName: 'report-sign-row',
	attributeBindings: ['signdata'],
	signdata: function() {
		if(!this.sign) {
			return '';
		} else {
			return this.sign.get('routerId');
		}
	}.property('sign'),

	click: function() {
		this.get('controller').send('signRowClicked', this.sign);
	},

	didInsertElement: function() {
		this.get('controller').on('updateSignRowClass', this, this.updateSignRowClass);
	},

	willDestroyElement: function() {
		this.get('controller').off('updateSignRowClass', this, this.updateSignRowClass);
	},

	updateSignRowClass: function(sign) {
		if(sign.id === this.sign.id) {
			this.set('selected', true);
		} else {
			this.set('selected', false);
		}
	}
});