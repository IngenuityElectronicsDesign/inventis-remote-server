export default Ember.View.extend({
	tagName: 'a',
	classNameBindings: ['mainClass', 'selected:active'],
	mainClass: 'list-group-item',
	selected: false,
	defaultTemplate: Ember.Handlebars.compile('Location: {{device.site}}'),
	device: null,
	//templateName: 'report-device-row',
	attributeBindings: ['devicedata'],
	devicedata: function() {
		if(!this.device) {
			return '';
		} else {
			return this.device.get('deviceIdentifier');
		}
	},

	click: function() {
		this.get('controller').send('deviceRowClicked', this.device);
	},

	didInsertElement: function() {
		this.get('controller').on('updateDeviceRowClass', this, this.updateDeviceRowClass);
	},

	willDestroyElement: function() {
		this.get('controller').off('updateDeviceRowClass', this, this.updateDeviceRowClass);
	},

	updateDeviceRowClass: function(device) {
		if(device.id === this.device.id) {
			this.set('selected', true);
		} else {
			this.set('selected', false);
		}
	}
});