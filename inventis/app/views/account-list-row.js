export default Ember.View.extend({
	templateName: 'account-list-row',
	email: '',
	click: function() {
		this.get('controller').send('rowClicked', this.email);
	}
});