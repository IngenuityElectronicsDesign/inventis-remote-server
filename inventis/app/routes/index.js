export default Ember.Route.extend({
	selectedTab: Ember.computed.alias('selected-tab.tab'),
	currentUserId: Ember.computed.alias('session.userId'),

	activate: function() {
		if(this.get('currentUserId') === -1) {
			this.transitionTo('login');
		} else {
			this.set('selectedTab', 'status');
			this.transitionTo('mainpage.status');
		}
	},
});
