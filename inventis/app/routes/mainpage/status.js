import MainPagesRoute from './mainpages';

export default MainPagesRoute.extend({
	model: function() {
		return this.store.find('alert-device');
	}
});