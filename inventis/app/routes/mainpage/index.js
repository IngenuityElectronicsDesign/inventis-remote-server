export default Ember.Route.extend({
	currentUserId: Ember.computed.alias('session.userId'),
	selectedTab: Ember.computed.alias('selected-tab.tab'),

	activate: function() {
		var id = this.get('currentUserId');
		if(id !== -1) {
			this.set('selectedTab', 'status');
			this.transitionTo('mainpage.status');
		} else {
			this.transitionTo("index");
		}
	}
});