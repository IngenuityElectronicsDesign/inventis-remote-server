export default Ember.Route.extend({
	currentUserId: Ember.computed.alias('session.userId'),
	activate: function() {
		if(this.get('currentUserId') === -1) {
			this.transitionTo('login');
		}
	},

	beforeModel: function() {
		if(this.get('currentUserId') === -1) {
			this.transitionTo('login');
		}
	}
});