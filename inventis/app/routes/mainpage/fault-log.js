/* global console */
import MainPagesRoute from './mainpages';

export default MainPagesRoute.extend({
	currentUserId: Ember.computed.alias('session.userId'),

	user: function() {
		return this.store.find('user', this.get('currentUserId'));
	}.property('currentUserId'),
	
	model: function() {
		return new Promise((function(resolve) {
			this.store.find('user', this.get('currentUserId'))
			.then(
				(function(user) {
					this.store.filter('header', {created: {'>=': user.get('logTime') || new Date() } }, 
						(function(header) {
							var headerCreated = header.get('created');
							var startDate = user.get('logTime') || new Date();
							var result = headerCreated.getTime() >= startDate.getTime();
							return result;
						}).bind(this))
						.then(
							function(headers){
								resolve(headers);
							}, function(reason) {
								console.log("Error getting Headers : " + JSON.stringify(reason));
								resolve([]);
							});
				}.bind(this)),
				function(reason) {
					console.log("Error getting User : " + JSON.stringify(reason));
					resolve([]);
				});
		}).bind(this));
	},

	setupController: function(controller, model) {
		controller.set('model', model);
	},

	events: {
		refreshModel: function() {
			this.refresh();
		}
	}
});