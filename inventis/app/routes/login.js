export default Ember.Route.extend({
	activate: function() {
		this.controllerFor('login').send('reset');
		this.controllerFor('login').send('resetAlert');
	}
});