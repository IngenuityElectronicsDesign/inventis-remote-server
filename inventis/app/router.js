var Router = Ember.Router.extend({
  location: ENV.locationType
});

Router.map(function() {
	this.resource('login');
	this.resource('forgot-password');
	this.resource('reset-password', {path: '/reset-password/:reset_id'});
	this.resource('mainpage', function() {
		this.route('accounts');
		this.route('status');
		this.route('device');
		this.route('report');
		this.route('help');
		this.route('fault-log');
	});
});

export default Router;
