export default DS.Model.extend({
	email: DS.attr('string'),
	logTime: DS.attr('date')
});