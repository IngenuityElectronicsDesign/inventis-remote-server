export default DS.Model.extend({
	protocolVersion: DS.attr('number'),
	deviceIdentifier: DS.attr('string'),
	authorisationData: DS.attr('string'),
	created: DS.attr('string'),
	command: DS.attr('string'),
	adcData: DS.belongsTo('header', {inverse: 'irsComm'}),
	checksum: DS.attr('number')
});