/* global console */
export default DS.Model.extend({
	owner: DS.belongsTo('alert-device', {async: true}),
	site: DS.attr('string'),
	project: DS.attr('string'),
	dest1: DS.attr('string'),
	dest2: DS.attr('string'),
	routerId: DS.attr('number'),
	latestLog: DS.belongsTo('log', {async: true}),
	active: function() {
		var promise = this.get('owner');
		promise.then((function(owner) {
			owner.get('latestHeader')
			.then((function(latestHeader) {
				latestHeader.get('routerLogs')
				.then((function(routerLogs) {
					var finds = {
						local: this.get('routerId'),
						length: routerLogs.content.length
					};
					// Ember doesn't populate the routerLogs but gives us the id of each log
					// so we need to perform a full lookup from the store.
					for(var i = 0; i < routerLogs.content.length; i++)
					{
						var log = routerLogs.objectAt(i);
						finds[i] = this.store.find('log', log.id);
					}
					Ember.RSVP.hash(finds).then((function(hash) {
						var found = false;
						for(var i = 0; i < hash.length; i++)
						{
							if(hash[i].get('routerId') === hash.local) {
								console.log("Status : " + JSON.stringify(hash[i].get('status')));
								found = hash[i].get('status').Active;
							}
						}
						this.set('active', found);
					}).bind(this));
				}).bind(this));
			}).bind(this));
		}).bind(this));
		return promise;
	}.property('owner.latestHeader.routerLogs.@each', 'owner.latestHeader.routerLogs.@each.status.Active')
});