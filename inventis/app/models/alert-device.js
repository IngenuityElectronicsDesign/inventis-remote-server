export default DS.Model.extend({
	deviceIdentifier: DS.attr('string'),
	site: DS.attr('string'),
	project: DS.attr('string'),
	dest1: DS.attr('string'),
	dest2: DS.attr('string'),
	latestHeader: DS.belongsTo('header', {async: true}),
	routers: DS.hasMany('adc-router', {async: true}),
	listName: function() {
		return this.get('site');
	}.property('site', 'dest1', 'dest2')
});