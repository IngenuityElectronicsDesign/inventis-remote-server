export default DS.Model.extend({
	routerId: DS.attr('number'),
	created: DS.attr('date'),
	isSign: DS.attr('boolean'),
	lampCurrent: DS.attr('raw'),
	lampOn: DS.attr('boolean'),
	upTime: DS.attr('number'),
	batteryVoltage: DS.attr(''),
	solarVoltage: DS.attr('number'),
	turbineVoltage: DS.attr('number'),
	ambientLight: DS.attr('number'),
	temperature: DS.attr('number'),
	track: DS.attr('number'),
	status: DS.attr('raw'),
	fault: DS.attr('raw'),
	header: DS.belongsTo('header', {inverse: 'routerLogs', async: true}),
	headerName: DS.attr('string'),
	sentToStreams: DS.attr('boolean'),
	statusString: function() {
		return JSON.stringify(this.get('status'));
	}.property('status'),
	faultString: function() { 
		return JSON.stringify(this.get('fault'));
	}.property('fault'),
	createdString: function() {
		return this.get('created').getDate() + "/" + (this.get('created').getMonth() + 1) + "/" + this.get('created').getFullYear() + " " + this.get('created').toLocaleTimeString();
	}.property('created'),
	lampCurrent1: function() {
		return this.get('lampCurrent')[0];
	}.property('lampCurrent'),
	lampCurrent2: function() {
		return this.get('lampCurrent')[1];
	}.property('lampCurrent')
});