export default DS.Model.extend({
	created: DS.attr('date'),
	localTimeOffset: DS.attr('number'),
	project: DS.attr('string'),
	site: DS.attr('string'),
	dest1: DS.attr('string'),
	dest2: DS.attr('string'),
	avgSpeed: DS.attr('number'),
	channel: DS.attr('number'),
	network: DS.attr('number'),
	softwareVersion: DS.attr('number'),
	versionFlag: DS.attr('number'),
	longitude: DS.attr('number'),
	latitude: DS.attr('number'),
	tracks: DS.attr('number'),
	trackDistance: DS.attr('raw'),
	attachedRouters: DS.attr('number'),
	attachedRouterData: DS.attr('raw'),
	onlineRouters: DS.attr('number'),
	systemFault: DS.attr('raw'),
	routerLogs: DS.hasMany('log', {inverse: 'header', async: true}),
	irsComm: DS.attr('number'),
	comms: function() {
		// If any are reporting a down status
		var commsError = false;
		this.get('routerLogs').forEach(function(log) {
			if(!log.get('status').Active)
				commsError = true;
		});
		if(commsError)
			return true;
		// If there aren't as many logs as attached routers, raise error
		var allLogs = this.get('routerLogs').length === this.get('attachedRouters');
		return allLogs;
	}.property('routerLogs.@each.status'),
	lampFailure: function() {
		return this.get('systemFault.BothLampsFailed') || this.get('systemFault.SingleLampFailure');
	}.property('systemFault.BothLampsFailed', 'systemFault.SingleLampFailure'),
	statusFailure: function() {
		return this.get('comms') || this.get('lampFailure') || this.get('systemFault.BatteryFailure');
	}.property('comms', 'lampFailure', 'systemFault.BatteryFailure'),
	statusWarning: function() {
		return this.get('systemFault.SolarFailure') || this.get('systemFault.DoorOpen');
	}.property('systemFault.SolarFailure', 'systemFault.DoorOpen'),
	systemFaultString: function() {
		return JSON.stringify(this.get('systemFault'));
	}.property('systemFault'),
	attachedRouterDataString: function() {
		return JSON.stringify(this.get('attachedRouterData'));
	}.property('attachedRouterData'),

	intervalObj: null
});