/* global console */
export default Ember.Controller.extend({
	loginFailed: false,
	isProcessing: false,
	currentUserId: Ember.computed.alias('session.userId'),

	actions: {
		login: function() {
			this.setProperties({
				loginFailed: false,
				isProcessing: true
			});

			var props = this.getProperties("email", "password");
			$.post('/session/create', props, (function(data) {
				console.log("New Session response : " + JSON.stringify(data));
				if(data.success) {
					// TODO : store the logged in User ID
					$("#alertDiv").removeClass("alert alert-info").html("");
					this.set('currentUserId', data.userId);
					this.transitionToRoute('mainpage');

				} else {
					this.send('reset');
					this.set('loginFailed', true);
					$("#alertDiv").removeClass("alert-info").addClass("alert-warning").html("Error logging in : " + data.err);
				}
			}).bind(this));
			$("#alertDiv").removeClass("alert-warning").addClass("alert alert-info").html("Logging in...");
		},

		reset: function() {
			this.setProperties({
				isProcessing: false
			});
		},

		resetAlert: function() {
			$("#alertDiv").removeClass('alert alert-info alert-warning').html("");
		},

		forgotPassword: function() {
			this.transitionToRoute('forgot-password');
		}
	}
});