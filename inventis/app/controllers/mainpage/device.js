/* global _ */
export default Ember.ArrayController.extend({
	selectedDevice: Ember.computed.alias('selected-adc.adc'),

	sortedModel: function() {
		var sorted = [];
		this.get('model').forEach(function(device) {
			var index = _.findIndex(sorted, function(wrapped) {
				return wrapped.project === device.get('project');
			});
			if(index === -1) {
				var newWrapped = {};
				newWrapped['project'] = device.get('project');
				newWrapped['data'] = [];
				sorted.push(newWrapped);
				index = sorted.length - 1;
			}
			sorted[index].data.push(device);
		});
		// Rename the known projects
		sorted.forEach(function(wrapped) {
			if(wrapped.project === 'P7524') {
				wrapped.project = 'Constrained Alignments';
			} else if( wrapped.project === 'P7516') {
				wrapped.project = 'Level Crossings';
			}
		});
		return sorted;
	}.property('model.@each'),

	actions: {
		rowClicked: function(location) {
			if(!location) {
				return;
			}
			this.set('selectedDevice', location);
			$("#device-list").find('a').each(function() {
				if(this.innerHTML.indexOf(location.get('listName')) > -1) {
					this.className = "list-group-item active";
				} else {
					this.className = "list-group-item";
				}
			});
		},

		pageLoaded: function() {
			var device = this.get('selectedDevice');
			this.send('rowClicked', device);
		}
	}
});