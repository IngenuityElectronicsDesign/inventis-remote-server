/* global console */
/* global confirm */
/* global prompt */
/* global alert */
export default Ember.ArrayController.extend({
	currentRow: '',
	currentUserId: Ember.computed.alias('session.userId'),

	actions: {
		rowClicked: function(email) {
			//console.log("Row was clicked : " + email);
			this.set('currentRow', email);
			$("#account-list").find('a').each(function() {
				if(this.innerHTML.indexOf(email) > -1) {
					this.className = "list-group-item active";
				} else {
					this.className = "list-group-item";
				}
			});
		},

		deleteClicked: function() {
			if(!this.get('currentRow') || this.get('currentRow') === '') {
				alert("Please select an account from the list before attempting to delete it.");
			} else {
				var shouldDelete = confirm("Are you sure you want to delete this account?\n"+this.get('currentRow'));
				if(shouldDelete) {
					var promiseArray = this.store.find('user', {email: this.get('currentRow')});
					promiseArray.then((function() {
						var length = promiseArray.get('length');
						if(length === 0) {
							console.log("Error : no account found for : " + this.get('currentRow'));
							$("#alertDiv").removeClass("alert-info")
							.addClass("alert alert-warning").html("Error : no account found for : " + this.get('currentRow'));
							return;
						} else if (length !== 1) {
							console.log("Error : More than one account found for : " + this.get('currentRow'));
							$("#alertDiv").removeClass("alert-info")
							.addClass("alert alert-warning").html("Error : More than one account found for : " + this.get('currentRow'));
							return;
						}

						console.log("Array length : " + promiseArray.get('length'));
						var arr = promiseArray.get('content');
						var account = arr.get('firstObject');
						account.destroyRecord();
						$("#alertDiv").removeClass("alert-info")
						.addClass("alert alert-success").html("Account deleted!");
					}).bind(this), (function(reason) {
						console.log("Error finding account with email : " + this.get('currentRow') + " : " + JSON.stringify(reason));
						$("#alertDiv").removeClass("alert-info")
						.addClass("alert alert-warning").html("Error finding account : " + JSON.stringify(reason));
					}).bind(this));
					$("#alertDiv").removeClass("alert-warning alert-success")
					.addClass("alert alert-info").html("Deleting from server...");
				} else {
					console.log("Should Not Delete");
				}


			}
		},

		createClicked: function() {
			var emailAddress = prompt("Enter Email Address for new Account");
			if(emailAddress !== null) {
				// so the server can assign ID, we do this via jquery
				var props = {email: emailAddress};
				$.post('/user/create', props, function(data) {
					console.log("Create account response : " + JSON.stringify(data));
					if(data.user) {
						$("#alertDiv").removeClass("alert-info")
						.addClass("alert alert-success").html("New User created successfully!");
					} else {
						$("#alertDiv").removeClass("alert-info")
						.addClass("alert alert-warning").html("Error creating account : " + JSON.stringify(data));
					}
				});
				$("#alertDiv").removeClass("alert-warning alert-success")
				.addClass("alert alert-info").html("Creating new acount on server...");
			}
		},

		resetPasswordClicked: function() {
			var promise = this.store.find('user', this.get('currentUserId'));
			promise.then(function(value) {
				var props = {
					email: value.get('email')
				};
				$.post('/user/forgotPassword', props, function(data) {
					console.log("Forgot Password Response : " + JSON.stringify(data));
					if(data.success) {
						$("#alertDiv").removeClass("alert-info")
						.addClass("alert alert-success").html("Reset password email sent.");
					} else {
						$("#alertDiv").removeClass("alert-info")
						.addClass("alert alert-warning").html("Error sending reset password email : " + JSON.stringify(data));
					}
				});
			}, function(reason) {
				console.log("Error finding current User : " + JSON.stringify(reason));
				$("#alertDiv").removeClass("alert-info")
				.addClass("alert alert-warning").html("Error sending reset password email : " + JSON.stringify(reason));
			});
			$("#alertDiv").removeClass("alert-warning alert-success")
			.addClass("alert alert-info").html("Sending Reset Password email...");
		}
	}
});