/* global console */
/* global moment */
export default Ember.ArrayController.extend(Ember.Evented, {
	selectedDeviceId: -1,
	selectedSignId: -1,
	startDate: moment(),
	endDate: moment(),
	foundLogs: [],
	sortedLogs: Ember.computed.sort('foundLogs', 'logSorting'),
	currentHeading: 'created',
	isAscending: true,
	logSorting: function() {
		var sort = this.get('currentHeading') + ":" + (this.get('isAscending') ? 'asc' : 'desc');
		return [sort];
	}.property('currentHeading', 'isAscending'),

	selectedDevice: function() {
		if(this.get('selectedDeviceId') === -1) {
			return {routers: []};
		}
		else {
			console.log("Finding Device : " + this.get('selectedDeviceId'));
			return this.store.find('alert-device', this.get('selectedDeviceId'));
		}
	}.property('selectedDeviceId'),

	selectedSign: function() {
		if(this.get('selectedSignId') === -1) {
			return {routerId: -1};
		} else {	
			console.log("Finding sign : " + this.get('selectedSignId'));
			return this.store.find('adc-router', this.get('selectedSignId'));
		}
	}.property('selectedSignId'),

	dateRange: function() {
		var range = this.get('startDate').toString() + " : " + this.get('endDate').toString();
		console.log("Range : " + range);
		return range;
	}.property('startDate', 'endDate'),

	logTimeClass: function() {
		if(this.get('currentHeading') === 'created') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	isSignClass: function() {
		if(this.get('currentHeading') === 'isSign') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	lampCurrent1Class: function() {
		if(this.get('currentHeading') === 'lampCurrent1') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	lampCurrent2Class: function() {
		if(this.get('currentHeading') === 'lampCurrent2') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	lampOnClass: function() {
		if(this.get('currentHeading') === 'lampOn') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	upTimeClass: function() {
		if(this.get('currentHeading') === 'upTime') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	batteryVoltageClass: function() {
		if(this.get('currentHeading') === 'batteryVoltage') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	solarVoltageClass: function() {
		if(this.get('currentHeading') === 'solarVoltage') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	turbineVoltageClass: function() {
		if(this.get('currentHeading') === 'turbineVoltage') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	ambientLightClass: function() {
		if(this.get('currentHeading') === 'ambientLight') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	temperatureClass: function() {
		if(this.get('currentHeading') === 'temperature') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	trackClass: function() {
		if(this.get('currentHeading') === 'track') {
			if(this.get('isAscending')) {
				return "glyphicon glyphicon-chevron-up";
			} else {
				return "glyphicon glyphicon-chevron-down";
			}
		} else {
			return "";
		}
	}.property('currentHeading', 'isAscending'),

	actions: {
		deviceRowClicked: function(device) {
			this.set('selectedSignId', -1);
			this.set('selectedDeviceId', device.id);
			this.trigger('updateDeviceRowClass', device);
			console.log("Device clicked : " + device.id);
		},

		signRowClicked: function(sign) {
			console.log("Controller sign row clicked");
			this.set('selectedSignId', sign.id);
			this.trigger('updateSignRowClass', sign);
		},

		startDateShown: function() {
			$("#startdatetimepicker").data("DateTimePicker").setDate(this.get('startDate'));
		},

		endDateShown: function() {
			$("#enddatetimepicker").data("DateTimePicker").setDate(this.get('endDate'));
		},

		startDateChanged: function() {
			this.set('startDate', $("#startdatetimepicker").data("DateTimePicker").getDate());
		},

		endDateChanged: function() {
			this.set('endDate', $("#enddatetimepicker").data("DateTimePicker").getDate());
		},

		updateReportClicked: function() {
			if(this.get('selectedSignId') === -1) {
				console.log("No sign has been selected yet");
				return;
			}
			var signPromise = this.get('selectedSign');
			var devicePromise = this.get('selectedDevice');
			var startPromise = this.get('startDate');
			var endPromise = this.get('endDate');
			Ember.RSVP.Promise.all([signPromise, devicePromise, startPromise, endPromise])
			.then((function(results) {
				var sign = results[0];
				var device = results[1];
				var startOrig = results[2];
				var endOrig = results[3];
				var start = startOrig.clone(); // We make clones so the original date field doesn't change.
				var end = endOrig.clone();
				//var diff = end.zone();
				//start.subtract('m', diff); // moment tells us GMT +10 is -600, so we subtract instead of add time diff.
				//end.subtract('m', diff);
				var promise = this.store.find('log', 
					{
						created: {
							'>=': start,
							'<=': end
						},
						limit: Math.pow(2, 53),
						routerId: sign.get('routerId'),
						headerName: device.get('site') + device.get('dest1') + device.get('dest2')
					});
				promise.then((function(results) {
					this.set('foundLogs', results);
				}).bind(this),
				function(error) {
					console.log("Error finding logs : " + JSON.stringify(error));
				});
			}).bind(this));
		},

		// Column header actions
		updateColumnSorting: function(heading) {
			var currentHeading = this.get('currentHeading');
			if(heading !== currentHeading) {
					// default to ascending when switching to a new column
					this.set('isAscending', true);
					this.set('currentHeading', heading);
			} else {
				// Just switch the asc/desc
				var isAscending = this.get('isAscending');
				this.set('isAscending', !isAscending);
			}
		},

		logTimeClicked: function() {
			this.send('updateColumnSorting', 'created');
		},

		isSignClicked: function() {
			this.send('updateColumnSorting', 'isSign');
		},
		
		lampCurrent1Clicked: function() {
			this.send('updateColumnSorting', 'lampCurrent1');
		},
		
		lampCurrent2Clicked: function() {
			this.send('updateColumnSorting', 'lampCurrent2');
		},
		
		lampOnClicked: function() {
			this.send('updateColumnSorting', 'lampOn');
		},
		
		upTimeClicked: function() {
			this.send('updateColumnSorting', 'upTime');
		},
		
		batteryVoltageClicked: function() {
			this.send('updateColumnSorting', 'batteryVoltage');
		},
		
		solarVoltageClicked: function() {
			this.send('updateColumnSorting', 'solarVoltage');
		},
		
		turbineVoltageClicked: function() {
			this.send('updateColumnSorting', 'turbineVoltage');
		},
		
		ambientLightClicked: function() {
			this.send('updateColumnSorting', 'ambientLight');
		},
		
		temperatureClicked: function() {
			this.send('updateColumnSorting', 'temperature');
		},
		
		trackClicked: function() {
			this.send('updateColumnSorting', 'track');
		},
		
	}
});