/* global console */
export default Ember.ArrayController.extend({
	currentUserId: Ember.computed.alias('session.userId'),

	actions: {
		clearClicked: function() {
			var promise = this.store.find('user', this.get('currentUserId'));
			promise.then((function (user) {
				user.set('logTime', new Date());
				user.save().then((function() {
					// User saved
					console.log("User Saved to server");
					this.get('target').send('refreshModel');
				}).bind(this), function(err) {
					// error
					console.log("Error saving User : " + JSON.stringify(err));
				});
			}).bind(this), function (err) {
				console.log("Error getting User : " + JSON.stringify(err));
			});
			
		}
	}
});