/* global console */
/* global io */
export default Ember.Controller.extend({
	actions: {
		resetPassword: function() {
			var props = this.getProperties("password", "confirmation");
			var model = this.get('model');
			var id = model.id;
			io.socket.post('/user/resetpassword/' + id, props, (function(data) {
				if(data.err || data.error) {
					console.log("Error resetting password: " + data.err);
					$("#alertDiv").removeClass("alert-info").addClass("alert-warning").html("There was an error resetting your password : " + JSON.stringify(data.err));
				} else {
					console.log("Password Reset");
					$("#alertDiv").removeClass("alert-info").addClass("alert-success").html("Your password has been reset. <a href='/login' class='alert-link'>Click here to return to the login page.</a>");
				}
			}).bind(this));
			$("#alertDiv").removeClass("alert-warning alert-success").addClass("alert alert-info").html("Submitting...");
		}
	}
});