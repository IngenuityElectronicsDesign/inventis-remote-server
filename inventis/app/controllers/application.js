export default Ember.Controller.extend({
	needs: ['mainpage'],

	currentUserId: Ember.computed.alias('session.userId'),
	selectedTab: Ember.computed.alias('selected-tab.tab'),

	currentPathDidChange: function() {
		var path = this.get('currentPath');
		var splits = path.split('.');
		for(var i = 0; i < splits.length; i++) {
			if(splits[i] === 'mainpage') {
				if(splits.length > i + 1) {
					// We are on a mainpage/something page
					this.set('selectedTab', splits[i + 1]);
				} else {
					// We are on mainpage so set it to status
					this.set('selectedTab', 'status');
				}
			}
		}
	}.observes('currentPath')
});