/* global console */
export default Ember.Controller.extend({
	currentUserId: Ember.computed.alias('session.userId'),
	selectedTab: Ember.computed.alias('selected-tab.tab'),

	monitorTabs: function() {
		this.send('updateTabs');
	}.observes('selectedTab'),

	actions: {
		accountsClicked: function() {
			//this.transitionToRoute('mainpage.accounts');
			console.log("Setting accounts");
			this.set('selectedTab', 'accounts');
		},

		statusClicked: function() {
			//this.transitionToRoute('mainpage.status');
			console.log("Setting status");
			this.set('selectedTab', 'status');
		},

		deviceClicked: function() {
			//this.transitionToRoute('mainpage.device');
			console.log("Setting device");
			this.set('selectedTab', 'device');
		},

		reportClicked: function() {
			//this.transitionToRoute('mainpage.report');
			console.log("Setting report");
			this.set('selectedTab', 'report');
		},

		faultLogClicked: function() {
			//this.transitionToRoute('mainpage.fault-log');
			console.log("Setting fault-log");
			this.set('selectedTab', 'fault-log');
		},

		helpClicked: function() {
			this.set('selectedTab', 'help');
		},

		logoutClicked: function() {
			$.getJSON('/session/destroy', (function(data) {
				if(data.sessionDestroyed) {
					this.set('currentUserId', -1);
					this.transitionToRoute('index');
				} else {
					console.log("Unknown error logging out");
				}
			}).bind(this));
		},

		updateTabs: function() {
			var tab = this.get('selectedTab');
			console.log("Updating tabs : " + tab);
			this.send('updateTab', tab, 'accounts');
			this.send('updateTab', tab, 'status');
			this.send('updateTab', tab, 'device');
			this.send('updateTab', tab, 'report');
			this.send('updateTab', tab, 'help');
			this.send('updateTab', tab, 'fault-log');
			this.transitionToRoute('mainpage.' + tab);
		},

		updateTab: function(selected, tabName) {
			if(selected === tabName) {
				$("#" + tabName + "Tab").addClass("active");
			} else {
				$("#" + tabName + "Tab").removeClass("active");
			}
		},
	}
});