/* global console */
export default Ember.Controller.extend({
	actions: {
		submitEmail: function() {
			var props = this.getProperties("email");
			$.post('/user/forgotPassword', props, (function(data) {
				if(data.err) {
					console.log("Error in forgot password : " + data.err);
					$("#alertDiv").removeClass("alert-info").addClass("alert-warning").html("There was an error resetting your password : " + JSON.stringify(data.err));
				} else {
					console.log("email sent");
					$("#alertDiv").removeClass("alert-info").addClass("alert-success").html("Your password has been reset.  Please check your email for instructions. <a href='/login' class='alert-link'>Click here to return to the login page.</a>");
				}
			}).bind(this));
			$("#alertDiv").removeClass("alert-warning alert-success").addClass("alert alert-info").html("Submitting...");
		}
	}
});