/* global isNaN */
/* global isFinite */
export default DS.RESTSerializer.extend({
	extractArray: function(store, primaryType, payload) {
		var newPayload = {};
		var plural = Ember.String.pluralize(primaryType.typeKey);
		var camel = Ember.String.camelize(primaryType.typeKey);
		newPayload[plural] = [];

		for(var prop in payload) {
			// If the property is a number (for a numbered array)
			if(!isNaN(parseFloat(prop)) && isFinite(prop)) {
				// Some of Ember's automated pulling collects individual 
				// get calls which are bundled in their own camelized object.
				// We check and pull them out if they are in those.
				var obj = payload[prop];
				if(obj.hasOwnProperty(camel)) {
					newPayload[plural].push(obj[camel]);
				} else {
					newPayload[plural].push(obj);
				}
			}
		}

		return this._super(store, primaryType, newPayload);
	}
});