/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var generatePassword = require('password-generator');

var sendEmail = function(id, email, callback, subject, template) {
	var options = {
		from: "Inventis Remote Server<donotreply@stategrowth.tas.gov.au>",
		to: {
			email: email
		},
		subject: subject,
		template: template
	}

	if(!sails.config.emailHostName) {
		console.log("You need to set up the emailHostName field in your local.js file correctly.  Defaulting to localhost");
	}

	var host = sails.config.emailHostName || 'http://localhost';

	var data = {
		resetURL: host + "/reset-password/" + id
	}

	var emailer = new Emailer(options, data);

	emailer.send(function(err, result) {
		callback(err, result);
	});
}

var sendForgotPasswordEmail = function(id, email, callback) {
	sendEmail(id, email, callback, "Inventis Remote Server Password Reset", "forgot-password");
}

var sendAccountCreatedEmail = function(id, email, callback) {
	sendEmail(id, email, callback, "New Account for Inventis Remote Server", "new-account");
}

module.exports = {
	create: function(req, res) {
		var userObj;
		var sendEmail = false;
		if(!req.param('password')) {
			// Generate a password 
			var pw = generatePassword();
			console.log("Password generated : " + pw);
			// We generate a random password so that the account is safe until
			// the user resets the password.
			userObj = {
				email: req.param('email'),
				password: pw,
				confirmation: pw
			}
			sendEmail = true;
		} else {
			userObj = {
				email: req.param('email'),
				password: req.param('password'),
				confirmation: req.param('confirmation')
			}
		}
		User.create(userObj, function userCreated(err, user) {
			if(err) {
				return res.json({
					err: err
				});
			}
			if(sendEmail) {
				var fpObj = {
					userId: user.id
				}
				ForgottenPassword.create(fpObj, function createdFP(err, forgottenPassword) {
					if(err) return res.json({err: err});
					sendAccountCreatedEmail(forgottenPassword.id, req.param('email'), function(err, result){
						if(err) return res.json({err: err});
						User.publishCreate(user);
						return res.json({
							user: user
						});
					});
				});
			} else {
				User.publishCreate(user);
				return res.json({
					user: user
				});
			}
		});
	},

	isLoggedIn: function(req, res) {
		if(req.session.authenticated) {
			res.json({
				isAuthenticated: true,
				userId: req.session.User.id
			});
		} else {
			res.json({
				isAuthenticated: false,
				userId: 0
			});
		}
	},

	forgotPassword: function(req, res) {
		if(!req.param('email')) {
			return res.json({
				err: "No Email address entered for reset"
			});
		}

		User.findOne({email: req.param('email')}, function foundUser(err, user) {
			if(err) return res.json({err: err});
			if(!user) return res.json({err: "No user account with that email address."});

			// Now check if there is already a reset password for this user
			
			ForgottenPassword.findOne({userId: user.id}, function foundFP(err, forgottenPassword) {
				if(err) return res.json({err: err});
				if(!forgottenPassword) {
					var fpObj = {
						userId: user.id
					}
					ForgottenPassword.create(fpObj, function createdFP(err, forgottenPassword) {
						if(err) return res.json({err: err});
						sendForgotPasswordEmail(forgottenPassword.id, req.param('email'), function(err, result) {
							if(err) {
								return res.json({
									err: err
								});
							} else {
								return res.json({
									success: true
								});
							}
						});
					});
				} else {
					// Already have an object so just resend email.
					sendForgotPasswordEmail(forgottenPassword.id, req.param('email'), function(err, result) {
						if(err) {
							return res.json({
								err: err
							});
						} else {
							return res.json({
								success: true
							});
						}
					});
				}
			});
		});
	},

	resetPassword: function(req, res) {
		if(!req.param('id')) {
			return res.json({
				err: "No ID for reset password provided."
			});
		}

		if(!req.param('password') || req.param('password') !== req.param('confirmation')) {
			return res.json({
				err: "Password and confirmation don't match."
			});
		}

		ForgottenPassword.findOne(req.param('id'), function foundFP(err, fp) {
			if(err) return res.json({err: err});
			if(!fp) return res.json({err: "No Forgotten Password object found for this id"});

			User.findOne({id: fp.userId}, function foundUser(err, user) {
				if(err) return res.json({err: err});
				if(!user) return res.json({err: "The user account no longer exists."});
				
				PasswordEncryptor.encrypt(req.param('password'))
				.then(function(encryptedPassword) {
					user.encryptedPassword = encryptedPassword;
					user.save(function saveComplete(err, userUpdated) {
						if(err) return res.json({err: err});
						return res.json({success: true});
					})
				})
				.fail(function(err) {
					return res.json({err: err});
				});
			})
		});
	},

	destroy: function(req, res) {
		User.findOne(req.param('id'))
		.exec(function(err, user) {
			if(err) return res.json({err: err});
			if(!user) return res.json({err: "User not found for id"});
			user.destroy(function(err) {
				console.log("User was destroyed : " + user.id);
				User.publishDestroy(user.id);
				if(req.session.authenticated) {
					if(req.session.User.id === user.id) {
						req.session.authenticated = false;
						delete req.session.User;
					}
				}
				// Ember data requires a 204 repsonse with an empty json object
				// for a valid delete.
				res.json(204, {});
			}); 
		});
	}
};
