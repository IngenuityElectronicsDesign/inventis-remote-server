/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	create: function(req, res) {
		if(!req.param('email') || !req.param('password')) {
			return res.json({
				success: false, 
				err: "Email and Password required",
				userId: 0
			});
		}

		User.findOne({email: req.param('email')}, function foundUser(err, user) {
			if(err) return res.json({success: false, err: err, userId: 0});
			if(!user) {
				return res.json({
					success: false,
					err: "No user account found for email : " + req.param('email'),
					userId: 0
				});
			}
			PasswordEncryptor.compare(req.param('password'), user.encryptedPassword)
			.then(function() {
				req.session.authenticated = true;
				req.session.User = user;
				return res.json({
					success: true,
					err: null,
					userId: user.id
				});
			})
			.fail(function(err) {
				return res.json({
					success: false,
					err: err,
					userId: 0
				});
			});
		});
	},

	destroy: function(req, res) {
		req.session.authenticated = false;
		delete req.session.User;
		return res.json({sessionDestroyed: true});
	}
};

