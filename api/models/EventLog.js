/**
* EventLog.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
      created: {
        type: 'datetime'
      },
      event: {
        type: 'string'
      },
      header: {
        model: 'Header'
      },
      headerName: {
        type: 'string'
      }
  }
};

