/**
* Log.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  	attributes: {

      routerId: {
        type: 'integer'
      },

      created: {
        type: 'datetime'
      },

      isSign: {
        type: 'boolean'
      },

      lampCurrent: {
        type: 'array'
      },

      lampOn: {
        type: 'boolean'
      },

      upTime: {
        type: 'integer'
      },

      batteryVoltage: {
        type: 'float'
      },

      solarVoltage: {
        type: 'float'
      },

      turbineVoltage: {
        type: 'float'
      },

      ambientLight: {
        type: 'integer'
      },

      temperature: {
        type: 'float'
      },

      track: {
        type: 'integer'
      },

      status: {
        type: 'json'
      },

      fault: {
        type: 'json'
      },

      header: {
        model: 'Header'
      },

      // This is an extra field populated on creation of the log 
      // to help searches in the Reports page.
      headerName: {
        type: 'string'
      },

      sentToStreams: {
        type: 'boolean',
        defaultsTo: false
      }
  	}
};

