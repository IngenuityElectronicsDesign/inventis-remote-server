/**
* Header.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  	attributes: {
  		created: {
  			type: 'datetime'
  		},

      localTimeOffset: {
        type: 'integer'
      },

      project: {
        type: 'string'
      },

  		site: {
  			type: 'string'
  		},

  		dest1: {
  			type: 'string'
  		},

  		dest2: {
  			type: 'string'
  		},

  		avgSpeed: {
  			type: 'float'
  		},

      channel: {
        type: 'integer'
      },

      network: {
        type: 'integer'
      },

      softwareVersion: {
        type: 'integer'
      },

      versionFlag: {
        type: 'integer'
      },

      longitude: {
        type: 'float'
      },

      latitude: {
        type: 'float'
      },

      tracks: {
        type: 'integer'
      },

      trackDistance: {
        type: 'array'
      },

      attachedRouters: {
        type: 'integer'
      },

      attachedRouterData: {
        type: 'array'
      },

      onlineRouters: {
        type: 'integer'
      },

  		systemFault: {
        type:'json'
      },

      routerLogs: {
  			collection: 'Log',
  			via: 'header'
  		},

      irsComm: {
        model: 'IrsComm',
        via: 'adcData'
      }
  	}
};

