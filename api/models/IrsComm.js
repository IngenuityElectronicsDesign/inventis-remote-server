module.exports = {

	attributes: {
		
		protocolVersion: {
			type: 'integer'
		},

		deviceIdentifier: {
			type: 'string'
		},

		authorisationData: {
			type: 'string'
		},

		created: {
			type: 'datetime'
		},

		command: {
			type: 'string'
		},

		adcData: {
			model: 'Header',
			via: 'irsComm'
		},

		checksum: {
			type: 'integer'
		}
	}
}