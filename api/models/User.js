/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  	attributes: {
  		email: {
	  		type: 'string',
	  		email: true,
	  		required: true,
	  		unique: true
	  	},

	  	encryptedPassword: {
	  		type: 'string'
	  	},

	  	logTime: {
	  		type: 'datetime'
	  	},

	  	toJSON: function() {
	  		var obj = this.toObject();
	  		delete obj.password;
	  		delete obj.confirmation;
	  		delete obj.encryptedPassword;
	  		delete obj._csrf;
	  		return obj;
	  	}
  	},

  	beforeCreate: function(values, next) {
  		if(!values.password || values.password !== values.confirmation) {
  			return next({err: "Password doesn't match password confirmation."});
  		}

  		values['logTime'] = new Date();
  		PasswordEncryptor.encrypt(values.password)
  		.then(function(encryptedPassword) {
  			values.encryptedPassword = encryptedPassword;
  			next();
  		})
  		.fail(function(err){
  			next(err);
  		});
  	}
};

