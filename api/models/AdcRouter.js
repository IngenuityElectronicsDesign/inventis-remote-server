/**
* Router.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  	attributes: {
  		owner: {
  			model: 'AlertDevice'
  		},

  		site: {
  			type: 'string'
  		},

  		project: {
  			type: 'string'
  		},

  		dest1: {
  			type: 'string'
  		},

  		dest2: {
  			type: 'string'
  		},

  		routerId: {
  			type: 'integer'
  		},

  		latestLog: {
  			model: 'Log'
  		}
  	}
};

