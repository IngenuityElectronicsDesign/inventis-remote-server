/**
* AlertDevice.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  	attributes: {
      deviceIdentifier: {
        type: 'string'
      },
      
  		site: {
  			type: 'string'
  		},

  		project: {
  			type: 'string'
  		},

  		dest1: {
  			type: 'string'
  		},

  		dest2: {
  			type: 'string'
  		},

  		latestHeader: {
  			model: 'Header'
  		},

  		routers: {
  			collection: 'AdcRouter',
  			via: 'owner'
  		},

  		faultLogEntryNumber: {
  			type: 'integer'
  		}

  	}
};

