/**
 * Module dependencies
 */
var util = require('util');



/**
 * Find One Record
 *
 * get /:modelIdentity/:id
 *
 * An API call to find and return a single model instance from the data adapter
 * using the specified id.
 *
 * Required:
 * @param {Integer|String} id  - the unique id of the particular instance you'd like to look up *
 *
 * Optional:
 * @param {String} callback - default jsonp callback param (i.e. the name of the js function returned)
 */

module.exports = function findOneRecord (req, res) {

  var Model = ActionUtil.parseModel(req);
  var pk = ActionUtil.requirePk(req);

  var query = Model.findOne(pk);
  query = ActionUtil.populateEach(query, req);
  query.exec(function found(err, matchingRecord) {
    if (err) return res.serverError(err);
    if(!matchingRecord) return res.notFound('No record found with the specified `id`.');

    matchingRecord = Populated.flatten(matchingRecord, Model);
    if (sails.hooks.pubsub && req.isSocket) {
      Model.subscribe(req, matchingRecord);
      ActionUtil.subscribeDeep(req, matchingRecord);
    }

    // We wrap the object based on it's camel case name so 
    // Ember is happy with the returning object.
    var globalId = Model.globalId.toString();
    if(globalId.length <= 1) {
      globalId = globalId.toLowerCase()
    } else {
      globalId = globalId.substr(0,1).toLowerCase() + globalId.substr(1);
    }

    var obj = {};
    obj[globalId] = matchingRecord;
    res.ok(obj);
  });

};