var net = require('net');
var Q = require('q');

var enableACDDataErrorTest = false;

var adcDataErrorCounter = 0;

var PROTOCOL_VERSION = 1;

var StateMachineEnum = {
	Idle : 0,
	WaitingForSetDateTimeSentCallback : 1,
	WaitingForSetDateTimeAckReceivedDataCallback : 2,
	WaitingForRequestStatusSentCallback : 3,
	WaitingForStatusResponseRecievedDataCallback : 4,
	CheckPendingSTREAMSCommands : 5,
	WaitingForRouterCommandSentCallback : 6,
	WaitingForRouterCommandAckReceivedDataCallback : 7,
	WaitingForEndSessionSentCallback : 8,
	WaitingForEndSessionAckReceivedDataCallback : 9,

	Count : 10
}

var StateMachineEnumStringArray = [
	"Idle",
	"WaitingForSetDateTimeSentCallback",
	"WaitingForSetDateTimeAckReceivedDataCallback",
	"WaitingForRequestStatusSentCallback",
	"WaitingForStatusResponseRecievedDataCallback",
	"CheckPendingSTREAMSCommands",
	"WaitingForRouterCommandSentCallback",
	"WaitingForRouterCommandAckReceivedDataCallback",
	"WaitingForEndSessionSentCallback",
	"WaitingForEndSessionAckReceivedDataCallback",

	"Count"
];

var CommsCommand = {
	SetDateTime : "SetDateTime",
	RequestStatus : "RequestStatus",
	EnableRouter : "EnableRouter",
	DisableRouter : "DisableRouter",
	EndSession : "EndSession",

	Count : "Count"
}

var CommsResponse = {
	StatusResponse : "StatusResponse",
	ACK : "ACK",
	NAK : "NAK",

	Count : "Count"
}


var NextStateCalledFromEnum = {
	Start : 0,
	Data : 1,
	WriteCallback : 2,
	NextState : 3,

	Count : 4
}

var NextStateCalledFromEnumStringArray = [
	"Start",
	"Data",
	"WriteCallback",

	"Count"
];

function ADCServerCode(c) {

	// state machine and state variables
	var commsState = StateMachineEnum.Idle;
	var deviceIdentifier = "?";
	var commsId = "?";
	var currentData = "";

	var Debug = function(debugOutput) {
		//console.log( "A[" + commsId + "] " + debugOutput );
	}

	var ErrorDebug = function(debugOutput) {
		console.log( "A[" + commsId + "] " + debugOutput );
	}

	var SaveDataLog = function(data) {
		// Temporary code to massage old format into new format
		var newIrsComm = {
			protocolVersion: data.ProtocolVersion,
			deviceIdentifier: data.DeviceIdentifier,
			created: new Date(data.Created*1000),		// convert seconds to msecs
			response: data.Response,
		}
		IrsComm.create(newIrsComm)
		.exec(function(err, irsComm) {
			if(err) return ErrorDebug("    Error creating IRS Comms object : " + JSON.stringify(err));
			var newHeader = {
				created: new Date(data.ADCData.Created*1000),	// convert seconds to msecs
				localTimeOffset: data.ADCData.LocalTimeOffset,
				project: data.ADCData.Project,
				site: data.ADCData.Site,
				dest1: data.ADCData.Dest1,
				dest2: data.ADCData.Dest2,
				avgSpeed: data.ADCData.AvgSpeed,
				channel: data.ADCData.Channel,
				network: data.ADCData.Network,
				softwareVersion: data.ADCData.SoftwareVersion,
				versionFlag: data.ADCData.VersionFlag,
				longitude: data.ADCData.Longitude,
				latitude: data.ADCData.Latitude,
				tracks: data.ADCData.Tracks,
				trackDistance: data.ADCData.TrackDistance,
				attachedRouters: data.ADCData.AttachedRouters,
				attachedRouterData: data.ADCData.AttachedRouterData,
				onlineRouters: data.ADCData.OnlineRouters,
				systemFault: data.ADCData.SystemFault,
				irsComm: irsComm.id
			}
			Header.create(newHeader)
			.exec(function(err, header) {
				if(err){
					return ErrorDebug("    Error creating header : " + JSON.stringify(err));
				}

				// Save the reference between the IRS Comms and the new Header
				IrsComm.update(irsComm.id, {adcData: header.id})
				.exec(function(err, updatedIrsComm) {
					if(err) {
						ErrorDebug("    Error updating IrsComm header ref : " + JSON.stringify(err));
					} else {
						//Debug("    Updated IrsComm properly");
					}
				});
				// We let control flow here rather than being dependent on updating IrsComm

				if(data.ADCData.EventLogs){
					Debug("EventLogs Found");
					var eventLogPromises = [];
					data.ADCData.EventLogs.forEach(function(l) {
						var newEventLog = {
							created: new Date(l.Created*1000),	// convert seconds to msecs
							event: l.Event,
							header: header.id,
							headerName: header.site + header.dest1 + header.dest2 
						}
						eventLogPromises.push(AddNewEventLog(newEventLog, header));
						
					});
					Q.allSettled(eventLogPromises)
					.then(HandleEventLogResults);
				}

				
				if(data.ADCData.RouterLogs) {
					var logPromises = [];
					data.ADCData.RouterLogs.forEach(function(l) {
						var newLog = {
							routerId: l.Id,
							created: new Date(l.Created*1000),	// convert seconds to msecs
							isSign: l.IsSign,
							lampCurrent: l.LampCurrent,
							lampOn: l.LampOn,
							upTime: l.UpTime,
							batteryVoltage: l.BatteryVoltage,
							solarVoltage: l.SolarVoltage,
							turbineVoltage: l.TurbineVoltage,
							ambientLight: l.AmbientLight,
							temperature: l.Temperature, 
							track: l.Track, 
							status: l.Status,
							fault: l.Fault,
							header: header.id,
							headerName: header.site + header.dest1 + header.dest2 
						}

						logPromises.push(AddNewLog(newLog, header, UpdateDevicesStatus));
						
					});
					Q.allSettled(logPromises)
					.then(HandleLogResults);
				} else {
					// If there are no attached signs, we jump straight through.
					UpdateDevicesStatus(header, []);
				}
			});
		});
		
	}

    var AddNewEventLog = function(newEventLog, header) {
        var deferred = Q.defer();
		EventLog.create(newEventLog, function(err, log) {
			if(err) {
				deferred.reject(new Error(err));
			} else {
				EventLog.publishCreate(log);
				deferred.resolve([header, log]);
			}
		});
        return deferred.promise;
    }

    var HandleEventLogResults = function(results) {
		results.forEach(function(result) {
			if(result.state === "fulfilled") {
				//Debug("    Created event log : " + result.value[1].event);
			} else {
				ErrorDebug("    Error creating event log : " + result.reason);
			}
		});
    }

    var AddNewLog = function(newLog, header, updateDevicesCallback) {
        var deferred = Q.defer();
		Log.create(newLog, function(err, log) {
			if(err) {
				deferred.reject(new Error(err));
			} else {
				Log.publishCreate(log);
				deferred.resolve([header, log, updateDevicesCallback]);
			}
		});
        return deferred.promise;
    }

    var HandleLogResults = function(results) {
        var logs = [];
        var header;
        var updateDeviceCallback;
		results.forEach(function(result) {
			if(result.state === "fulfilled") {
				// result array is [header, log, callback]
				logs.push(result.value[1]);
                if(!header) {
                    header = result.value[0];
                }
                if(!updateDeviceCallback) {
                    updateDeviceCallback = result.value[2];
                }
			} else {
				ErrorDebug("    Error creating log : " + result.reason);
			}
		});

		// Grab the populated header now the logs are all linked and publish
		Header.findOne(header.id)
		.populate('routerLogs')
		.populate('irsComm')
		.exec(function foundHeader(err, populatedHeader) {
			if(err) {
				// Do nothing?
				ErrorDebug("    Error retrieving completed header object for publishing : " + JSON.stringify(err));
			} else if(!populatedHeader) {
				ErrorDebug("    Didn't find populated header");
			} else {
				var flatPopulatedHeader = Populated.flatten(populatedHeader, Header);
				//Debug("    Publishing Header");
				Header.publishCreate(flatPopulatedHeader);
			}
		});

		if(updateDeviceCallback) {
            updateDeviceCallback(header, logs);
        }
    }

	var UpdateDevicesStatus = function(header, logs) {
		// find the AlertDeviceController
		AlertDevice.findOne({deviceIdentifier:header.site + "_" + header.dest1 + "_" + header.dest2})
		.populate('routers')
        .populate('latestHeader')
		.exec(function foundAlertDevice(err, alertDevice) {
			if(err) {
				return ErrorDebug("    Error finding Alert Device : " + JSON.stringify(err));
			}
			if(!alertDevice) {
				// Create a new Alert Device
				var newAlertDevice = {
					deviceIdentifier: header.site + "_" + header.dest1 + "_" + header.dest2,
					site: header.site,
					project: header.project,
					dest1: header.dest1,
					dest2: header.dest2,
					latestHeader: header.id
				}
				AlertDevice.create(newAlertDevice)
				.exec(function(err, alertDevice) {
					if(err) {
						ErrorDebug("    Error creating new AlertDevice : " + JSON.stringify(err));
					} else if(!alertDevice) {
						ErrorDebug("    Error creating new AlertDevice : No AlertDevice object passed back.");
					} else {
						commsId = alertDevice.id;
						Debug("    New alertDevice created - commsId set to:" + commsId);
						Debug("    Publishing Alert Device Create : " + JSON.stringify(alertDevice));
						AlertDevice.publishCreate(alertDevice);
						UpdateRoutersStatus(alertDevice, logs);
					}
				});
			} else {
				// Update the Alert Device
				//Debug("    Old header Id : " + alertDevice.latestHeader.id + " : new header Id : " + header.id);
				AlertDevice.update(alertDevice.id, {latestHeader: header.id})
				.exec(function(err, updatedAlertDevices) {
					if(err) {
						ErrorDebug("    Error updating AlertDevice : " + JSON.stringify(err));
					} else if(!updatedAlertDevices) {
						ErrorDebug("    Error updating AlertDevice : No updated AlertDevice object passed back.");
					} else {
						AlertDevice.findOne(updatedAlertDevices[0].id)
						.populate('routers')
                        .populate('latestHeader')
						.exec(function found(err, updatedPopulatedAlertDevice) {
							if(err) ErrorDebug("    Error finding Alert Device after update");
							updatedPopulatedFlattenedAlertDevice = Populated.flatten(updatedPopulatedAlertDevice, AlertDevice);
							AlertDevice.publishUpdate(updatedPopulatedAlertDevice.id, updatedPopulatedFlattenedAlertDevice, null, {previous: alertDevice});
							UpdateRoutersStatus(updatedPopulatedFlattenedAlertDevice, logs);
						})
					}
				});
			}
		});
	}

	var UpdateRoutersStatus = function(alertDevice, logs) {
		if(!logs || logs.length === 0)
			return;
		logs.forEach(function(l) {
			//Debug("    Update Router Status:" + l.id);
			AdcRouter.findOne({site:alertDevice.site, project:alertDevice.project, dest1:alertDevice.dest1, dest2:alertDevice.dest2, routerId:l.routerId})
			.populate('latestLog')
			.exec(function(err, router) {
				if(err) {
					ErrorDebug("    Error finding AdcRouter object : " + JSON.stringify(err));
				} else if(!router) {
					// Create a new router
					var newRouter = {
						owner: alertDevice.id,
						site: alertDevice.site,
						project: alertDevice.project,
						dest1: alertDevice.dest1,
						dest2: alertDevice.dest2,
						routerId: l.routerId,
						latestLog: l.id
					}
					AdcRouter.create(newRouter)
					.exec(function(err, router) {
						if(err) {
							ErrorDebug("    Error creating new AdcRouter : " + JSON.stringify(err));
						} else if(!router) {
							ErrorDebug("    Error creating new AdcRouter : No router object returned");
						} else {
							Debug("    Publishing AdcRouter Create : " + JSON.stringify(router));
							AdcRouter.publishCreate(router);
						}
					});
				} else {
					// Update the existing router
					//Debug("LT:" + (new Date(router.latestLog.created)).getTime() + " NT:" + (new Date(l.created)).getTime() );
					if ( !router ) {
						Debug("router == null");
					} else if( !router.latestLog || (new Date(router.latestLog.created)).getTime() < (new Date(l.created)).getTime() ) {
						AdcRouter.update(router.id, {latestLog: l.id})
						.exec(function(err, updatedRouters) {
							if(err) {
								ErrorDebug("    Error updating AdcRouter : " + JSON.stringify(err));
							} else if(!updatedRouters) {
								ErrorDebug("    Error updating AdcRouter : No Updated object was returned");
							} else {
								AdcRouter.publishUpdate(updatedRouters[0].id, updatedRouters[0]);
							}
						});
					} else {
						// Time was older, so no update
						Debug("    New log was older than existing log.  Skipping...");
						Debug("    router : " + router.latestLog.created.toUTCString() + " : l : " + l.created.toUTCString());					}
				}
			});
		});
	}

	var SetCommsState = function(newState,errorMessage) {
		if ( errorMessage != null) {
			ErrorDebug("x-- " + errorMessage + " - " + StateMachineEnumStringArray[commsState] + " -> " + StateMachineEnumStringArray[newState] );
		} else {
			//Debug("--- " + StateMachineEnumStringArray[commsState] + " -> " + StateMachineEnumStringArray[newState] );
			Debug("--- " + StateMachineEnumStringArray[newState] );
		}
		commsState = newState;
	}

	var Send = function(command,parameter) {
		var sendString = command;
		if ( parameter != null ) {
			sendString+= ',' + parameter;
		}
		Debug("    Send: " + sendString);
		sendString+= '\r\n';
		c.write(sendString,WriteCallback);
	}

	var SetNextCommsState = function(calledFrom,jsonDataIn) {
		//Debug("    SetNextCommsState calledFrom:" + NextStateCalledFromEnumStringArray[calledFrom] );
		switch (commsState ) {
			case StateMachineEnum.Idle:
				if (calledFrom==NextStateCalledFromEnum.Start) {
					SetCommsState( StateMachineEnum.WaitingForSetDateTimeSentCallback );
					// send 
					var nowDate = new Date();
					var msecsDate = nowDate.getTime();
					Send( CommsCommand.SetDateTime, Math.floor(msecsDate/1000).toString() );
				} else {
					SetCommsState( StateMachineEnum.Idle, "Idle - not called from Start" );
				}
				break;
			case StateMachineEnum.WaitingForSetDateTimeSentCallback:
				if (calledFrom==NextStateCalledFromEnum.WriteCallback) {
					SetCommsState( StateMachineEnum.WaitingForSetDateTimeAckReceivedDataCallback );
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForSetDateTimeSentCallback - not called from WriteCallback:" + calledFrom );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForSetDateTimeAckReceivedDataCallback:
				if (calledFrom==NextStateCalledFromEnum.Data) {
					if ( jsonDataIn!=null) {
						if ( jsonDataIn.hasOwnProperty("Response") ) {
							if ( jsonDataIn.Response == CommsResponse.ACK ) {
								// we received a status response
								//Debug("jsonDataIn:" + JSON.stringify(jsonDataIn, null, '\t'));
								SetCommsState( StateMachineEnum.WaitingForRequestStatusSentCallback );
								Send( CommsCommand.RequestStatus, null );
							} else {
								SetCommsState( StateMachineEnum.Idle, "WaitingForSetDateTimeAckReceivedDataCallback - did not receive Status Response message" );
								SetNextCommsState(NextStateCalledFromEnum.Start,null);
							}
						} else {
							SetCommsState( StateMachineEnum.Idle, "WaitingForSetDateTimeAckReceivedDataCallback - no Response property in data" );
							SetNextCommsState(NextStateCalledFromEnum.Start,null);
						}
					} else {
						SetCommsState( StateMachineEnum.Idle, "WaitingForSetDateTimeAckReceivedDataCallback - null data" );
						SetNextCommsState(NextStateCalledFromEnum.Start,null);
					}
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForSetDateTimeAckReceivedDataCallback - not called from Data:" + calledFrom );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForRequestStatusSentCallback:
				if (calledFrom==NextStateCalledFromEnum.WriteCallback) {
					SetCommsState(StateMachineEnum.WaitingForStatusResponseRecievedDataCallback);
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForRequestStatusSentCallback - not called from WriteCallback:" + calledFrom );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForRequestStatusSentCallback:
				if (calledFrom==NextStateCalledFromEnum.WriteCallback) {
					SetCommsState(StateMachineEnum.WaitingForStatusResponseRecievedDataCallback);
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForRequestStatusSentCallback - not called from WriteCallback:" + calledFrom );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForStatusResponseRecievedDataCallback:
				if (calledFrom==NextStateCalledFromEnum.Data) {
					if ( jsonDataIn!=null) {
						if ( jsonDataIn.hasOwnProperty("Response") ) {
							if ( jsonDataIn.Response == CommsResponse.StatusResponse ) {
								// we received a status response
								//Debug("jsonDataIn:" + JSON.stringify(jsonDataIn, null, '\t'));
								SaveDataLog(jsonDataIn);
								// Check if we have any pending STREAMS commands to send to this ADC
								SetCommsState( StateMachineEnum.CheckPendingSTREAMSCommands );
								SetNextCommsState(NextStateCalledFromEnum.NextState,null);
							} else {
								SetCommsState( StateMachineEnum.Idle, "WaitingForStatusResponseRecievedDataCallback - did not receive Status Response message" );
								SetNextCommsState(NextStateCalledFromEnum.Start,null);
							}
						} else {
							SetCommsState( StateMachineEnum.Idle, "WaitingForStartSessionReceivedDataCallback - no Response property in data" );
							SetNextCommsState(NextStateCalledFromEnum.Start,null);
						}
					} else {
						SetCommsState( StateMachineEnum.Idle, "WaitingForStatusResponseRecievedDataCallback - null data" );
						SetNextCommsState(NextStateCalledFromEnum.Start,null);
					}
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForStatusResponseRecievedDataCallback - not called from Data" );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.CheckPendingSTREAMSCommands:
				if (calledFrom==NextStateCalledFromEnum.NextState) {
					/* DISABLED until we have a mechainsim in STREAMS we can actually test this with!
					// Check to see if we have any pending STREAMS commands to send to this device
					if ( !DisableRouterSent ) {
						DisableRouterSent = true;
						SetCommsState( StateMachineEnum.WaitingForRouterCommandSentCallback );
						Send( CommsCommand.DisableRouter, "0" );
					} else if ( !EnableRouterSent ) {
						EnableRouterSent = true;
						SetCommsState( StateMachineEnum.WaitingForRouterCommandSentCallback );
						Send( CommsCommand.EnableRouter, "0" );
					} else {
						// we are done, end the session
						SetCommsState( StateMachineEnum.WaitingForEndSessionSentCallback );
						Send( CommsCommand.EndSession, null );
					}
					*/
					// we are done, end the session
					SetCommsState( StateMachineEnum.WaitingForEndSessionSentCallback );
					Send( CommsCommand.EndSession, null );
				} else {
					SetCommsState( StateMachineEnum.Idle, "CheckPendingSTREAMSCommands - not called from NextState" );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForRouterCommandSentCallback:
				if (calledFrom==NextStateCalledFromEnum.WriteCallback) {
					SetCommsState(StateMachineEnum.WaitingForRouterCommandAckReceivedDataCallback);
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForRouterCommandSentCallback - not called from WriteCallback" );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForRouterCommandAckReceivedDataCallback:
				if (calledFrom==NextStateCalledFromEnum.Data) {
					if ( jsonDataIn!=null) {
						if ( jsonDataIn.hasOwnProperty("Response") ) {
							if ( jsonDataIn.Response == CommsResponse.ACK ) {
								// we received an ACK
								// go back and check if we have any additional commands to send to this ADC
								Debug("Disable / Enable Device Command ACK received");
								SetCommsState( StateMachineEnum.CheckPendingSTREAMSCommands, null );
								SetNextCommsState(NextStateCalledFromEnum.NextState,null);
							} else {
								SetCommsState( StateMachineEnum.Idle, "WaitingForRouterCommandAckReceivedDataCallback - did not receive ACK" );
								SetNextCommsState(NextStateCalledFromEnum.Start,null);
							}
						} else {
							SetCommsState( StateMachineEnum.Idle, "WaitingForRouterCommandAckReceivedDataCallback - no Response property in data" );
							SetNextCommsState(NextStateCalledFromEnum.Start,null);
						}
					} else {
						SetCommsState( StateMachineEnum.Idle, "WaitingForRouterCommandAckReceivedDataCallback - null data" );
						SetNextCommsState(NextStateCalledFromEnum.Start,null);
					}
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForRouterCommandAckReceivedDataCallback - not called from Data" );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForEndSessionSentCallback:
				if (calledFrom==NextStateCalledFromEnum.WriteCallback) {
					SetCommsState(StateMachineEnum.WaitingForEndSessionAckReceivedDataCallback);
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForEndSessionSentCallback - not called from WriteCallback" );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			case StateMachineEnum.WaitingForEndSessionAckReceivedDataCallback:
				if (calledFrom==NextStateCalledFromEnum.Data) {
					if ( jsonDataIn!=null) {
						if ( jsonDataIn.hasOwnProperty("Response") ) {
							if ( jsonDataIn.Response == CommsResponse.ACK ) {
								// we received an ACK
								// our session will end - terminated by the client shortly
								SetCommsState( StateMachineEnum.Idle, null );
							} else {
								SetCommsState( StateMachineEnum.Idle, "WaitingForEndSessionAckReceivedDataCallback - did not receive ACK" );
								SetNextCommsState(NextStateCalledFromEnum.Start,null);
							}
						} else {
							SetCommsState( StateMachineEnum.Idle, "WaitingForStartSessionReceivedDataCallback - no Response property in data" );
							SetNextCommsState(NextStateCalledFromEnum.Start,null);
						}
					} else {
						SetCommsState( StateMachineEnum.Idle, "WaitingForEndSessionAckReceivedDataCallback - null data" );
						SetNextCommsState(NextStateCalledFromEnum.Start,null);
					}
				} else {
					SetCommsState( StateMachineEnum.Idle, "WaitingForEndSessionAckReceivedDataCallback - not called from Data" );
					SetNextCommsState(NextStateCalledFromEnum.Start,null);
				}
				break;
			default:
				SetCommsState( StateMachineEnum.Idle, "Error - Receive Data - unknown commsState" + commsState );
				SetNextCommsState(NextStateCalledFromEnum.Start,null);
				break;
		}
	}

	var WriteCallback = function() {
		//Debug("    WriteCallback");
		SetNextCommsState(NextStateCalledFromEnum.WriteCallback,null);
	}

	// kick off the state machine
	Debug("Server Created");
	SetNextCommsState(NextStateCalledFromEnum.Start,null);

	// setup all of the event handlers
	c.on('end', function() {
		Debug("Server disconnected");
		commsState = StateMachineEnum.WaitingForStartSessionReceivedDataCallback;
	});

	c.on('connect', function() {
		Debug("New connection");
	});

	c.on('data', function(buffer) {
		if ( enableACDDataErrorTest && adcDataErrorCounter++ > 5 ) {
			Debug("    ---- inject Error" );
			currentData += "BadData";
			adcDataErrorCounter = 0;
		} else {
			currentData += buffer.toString();
		}
		try {
			var jsonData = JSON.parse(currentData);
			if ( jsonData != null ) {
				Debug("    Data In: " + currentData);
				Debug("    JSON In: " + JSON.stringify(jsonData,null,'\t') );
				if ( jsonData.hasOwnProperty("DeviceIdentifier") ) {
					// check and set set the current device (prefixed to our debug messages)
					if ( deviceIdentifier == "?" ) {
						deviceIdentifier = jsonData.DeviceIdentifier;
						Debug("    DeviceIdentifier:" + deviceIdentifier );
						AlertDevice.findOne({deviceIdentifier:deviceIdentifier})
						.exec(function(err,alertDevice) {
							if (err) {
								ErrorDebug("    Error - AlertDevice search error:" + JSON.stringify(err));
							} else if (!alertDevice) {
								ErrorDebug("    New Device - using incrementing index");
							} else {
								commsId = alertDevice.id;
								Debug("    CommsId set to:" + commsId);
							}
							if ( deviceIdentifier == jsonData.DeviceIdentifier ) {
								// deviceID is valid pass the object on
								Debug("    Received Response: " + jsonData.Response);
								SetNextCommsState(NextStateCalledFromEnum.Data,jsonData);
							} else {
								SetCommsState( StateMachineEnum.Idle, "Data Rec - deviceIdentifier changed!!??" );
							}
						});
					} else {
						if ( deviceIdentifier == jsonData.DeviceIdentifier ) {
							// deviceID is valid pass the object on
							Debug("    Received Response: " + jsonData.Response);
							SetNextCommsState(NextStateCalledFromEnum.Data,jsonData);
						} else {
							SetCommsState( StateMachineEnum.Idle, "Data Rec - deviceIdentifier changed!!??" );
						}
					}
				} else {
					SetCommsState( StateMachineEnum.Idle, "Data Rec - no DeviceIdentifier property in data" );
				}
			} else {
				SetCommsState( StateMachineEnum.Idle, "Data Rec - null from JSON.parse" );
			}
			//Debug("    Complete JSON object. : " + currentData);			
			// If it didn't throw an error, we had a full object, so we need to reset the data
			currentData = "";
		}
		catch(err) {
			// json data wasn't ready yet.
			// This isn't an error - it just means all the data isn't here yet... DON'T use ErrorDebug
			Debug("    Incomplete JSON object: " + currentData);
		}
	});

	c.on('timeout', function() {
		Debug("Connection timeout");
		commsState = StateMachineEnum.Idle;
	});

	c.on('drain', function() {
		Debug("Connection drain");
	});

	c.on('error', function(error){
		Debug("Connection error : " + JSON.stringify(error));
		commsState = StateMachineEnum.Idle;
	});

	c.on('close', function(hadError) {
		Debug("Connection closed : " + hadError);
		commsState = StateMachineEnum.Idle;
	})

}

var ADCCommsServer = {
	server:null,
	initServer: function() {
		if(this.server)
			return;
		this.server = net.createServer(ADCServerCode);
		this.server.listen(1338);
		setInterval(this.pollForTimeouts.bind(this), 20000);
		//setTimeout(this.pollForTimeouts.bind(this), 20000);
		console.log("Init ADC Server on : 1338");
	},

	pollForTimeouts: function() {
		//console.log("Polling");
		AlertDevice.find()
		.populate('routers')
		.populate('latestHeader')
		.exec(function(err, alertDevices) {
			alertDevices.forEach(function(alertDevice){
				var logFindPromises = [];
				alertDevice.routers.forEach(function(router) {
					var deferred = Q.defer();
					Log.findOne(router.latestLog) 
					.exec(function(err, latestLog) {
						if(err) {
							deferred.reject(err);
						} else {
							deferred.resolve(latestLog);
						}
					});
					logFindPromises.push(deferred.promise);
				}, this);
				Q.allSettled(logFindPromises)
				.then(function(results) {
					var updatingLogs = [];
					var maintainingLogs = [];
					results.forEach(function(result) {
						if(result.state === "fulfilled") {
							var now = new Date();
							var diff = now - result.value.created;
							var maxDiff = 90 * 60 * 1000;
							// If it is over time, and currently active, 
							// then we mark active as false and create a 
							// new log.
							if(diff > maxDiff && result.value.status.Active) {
								updatingLogs.push(result.value);
							} else {
								maintainingLogs.push(result.value);
							}
						}
					});

					// if there are no updating logs, then we are done here
					if(updatingLogs.length === 0) {
						return; // console.log("No updating logs for alert device : " + site + dest1 + dest2);
					}

					// We first create a IrsComm object
					IrsComm.findOne(alertDevice.latestHeader.irsComm)
					.exec(function(err, irsComm) {
						// Handle undefined return object
						var irsCommEdit;
						if(!irsComm)
						{
							irsCommEdit = {};
						}
						else
						{
							var stringifiedIrsComm = JSON.stringify(irsComm);
							var irsCommEdit = JSON.parse(stringifiedIrsComm);
							// Remove any fields we don't include in the save data code above.
							delete irsCommEdit.authorisationData;
							delete irsCommEdit.command;
							delete irsCommEdit.checksum;
							// Removed server generated fields and links
							delete irsCommEdit.id;
							delete irsCommEdit.adcData;
							delete irsCommEdit.createdAt;
							delete irsCommEdit.updatedAt;
							// Update created date
							irsCommEdit.created = new Date();
						}
						// Update created date
						irsCommEdit.created = new Date();
						
						IrsComm.create(irsCommEdit)
						.exec(function(err, newIrsComm) {
							// Now create new header
							var headerEdit = JSON.parse(JSON.stringify(alertDevice.latestHeader));
							//console.log("Header Before : " + JSON.stringify(headerEdit, undefined, 2));
							// Update creadted time
							headerEdit.created = new Date();
							// Update irsComm to new one
							headerEdit.irsComm = newIrsComm.id;
							// Remove fields
							delete headerEdit.routerLogs;
							delete headerEdit.id;
							delete headerEdit.createdAt;
							delete headerEdit.updatedAt;
							//console.log("Header After : " + JSON.stringify(headerEdit, undefined, 2));
							Header.create(headerEdit)
							.exec(function(err, newHeader) {
								IrsComm.update(newIrsComm.id, {adcData: newHeader.id})
								.exec(function(err, updatedIrsComm) {
									// Don't need to do anything here
								});

								// Now we have a header we can attach our updating and maintaining 
								// logs to
								var logCreatePromises = [];
								// Now create new logs for the updating logs;
								updatingLogs.forEach(function(log) {
									var deferred = Q.defer();
									var editLog = JSON.parse(JSON.stringify(log));

									editLog.created = new Date();
									editLog.status.Active = false;
									editLog.sentToStreams = false;
									editLog.header = newHeader.id;
									delete editLog.id;
									delete editLog.createdAt;
									delete editLog.updatedAt;

									Log.create(editLog)
									.exec(function(err, newLog) {
										if(err) {
											deferred.reject(err);
										} else {
											deferred.resolve(newLog);
										}
									});
									logCreatePromises.push(deferred.promise);
								});
								Q.allSettled(logCreatePromises)
								.then(function(results) {
									var newLogs = [];
									results.forEach(function(result) {
										if(result.state === "fulfilled") {
											// The log was created fine
											newLogs.push(result.value);
										} else {
											console.log("Error created new log : " + JSON.stringify(result.value));
										}
									});
									
									// Now clone any maintaining logs and direct them to our new header
									var logUpdatePromises = [];
									maintainingLogs.forEach(function(log) {
										var editLog = JSON.parse(JSON.stringify(log));
										editLog.header = newHeader.id;
										delete editLog.id;
										delete editLog.createdAt;
										delete editLog.updatedAt;
										var deferred = Q.defer();
										Log.create(editLog)
										.exec(function(err, newMaintainLog) {
											if(err) {
												deferred.reject(err);
											} else {
												deferred.resolve(newMaintainLog);
											}
										});
										logUpdatePromises.push(deferred.promise);
									});
									Q.allSettled(logUpdatePromises)
									.then(function(results) {
										results.forEach(function(result) {
											if(result.state === "fulfilled") {
												newLogs.push(result.value);
											} else {
												console.log("Error cloning log : " + JSON.stringify(err));
											}
										});
										// We now have all the logs created!
										var updatePromises = [];
										// Update AlertDevice with new header
										var alertDeferred = Q.defer();
										AlertDevice.update(alertDevice.id, {latestHeader: newHeader.id})
										.exec(function(err, updatedAlertDevices) {
											if(err) {
												alertDeferred.reject(err);
											} else {
												alertDeferred.resolve(updatedAlertDevices[0]);
											}
										});
										updatePromises.push(alertDeferred.promise);
										// Update AdcRouters
										alertDevice.routers.forEach(function(router) {
											var deferred = Q.defer();
											var index = -1;
											for(var i = 0; i < newLogs.length; i++)
											{
												if(newLogs[i].routerId === router.routerId) {
													index = i;
												}
											}
											if(index === -1) {
												deferred.reject("Error, log wasn't available for router : " + router.id);
											} else {
												AdcRouter.update(router.id, {latestLog: newLogs[index].id})
												.exec(function(err, updatedRouters) {
													if(err) {
														deferred.reject(err);
													} else {
														deferred.resolve(updatedRouters[0]);
													}
												});
											}
											updatePromises.push(deferred.promise);
										});

										Q.allSettled(updatePromises)
										.then(function(results) {
											// Now publish updates
											results.forEach(function(result) {
												if(result.state === "fulfilled") {
													if(result.value.hasOwnProperty('latestHeader')) {
														// It's the AlertDevice
														AlertDevice.findOne(result.value.id)
														.populate('routers')
								                        .populate('latestHeader')
								                        .exec(function found(err, updatedPopulatedAlertDevice) {
								                        	if(err) return console.log("    Error finding Alert Device after update");
								                        	var flattened = Populated.flatten(updatedPopulatedAlertDevice, AlertDevice);
								                        	AlertDevice.publishUpdate(flattened.id, flattened, null, {previous: alertDevice});
								                        });
													} else {
														// It's an AdcRouter
														AdcRouter.publishUpdate(result.value.id, result.value);
													}
												} else {
													console.log("Error updating device or router : " + JSON.stringify(result.value));
												}
											})
										});
									});
								});
							});
						});
					});
				}.bind(this));
			}, this);
		}.bind(this));
	}
};

module.exports = ADCCommsServer;
