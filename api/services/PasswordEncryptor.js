var bcrypt = require('bcrypt-nodejs');
var Q = require('q');

module.exports = {
	encrypt: function(password) {
		var deferred = Q.defer();
		bcrypt.hash(password, null, null, function passwordEncrypted(err, encPassword) {
			if(err) {
				deferred.reject(err);
			} else {
				deferred.resolve(encPassword);
			}
		});
		return deferred.promise;
	},

	compare: function(password, encryptedPassword) {
		var deferred = Q.defer();

		bcrypt.compare(password, encryptedPassword, function(err, res) {
			if(err) {
				deferred.reject(err);
			} else {
				if(res) {
					deferred.resolve();
				} else {
					deferred.reject("Passwords don't match");
				}
				
			}
		})
		return deferred.promise;
	}
};