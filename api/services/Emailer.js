var emailer = require('nodemailer');
var fs = require('fs');
var _ = require('underscore');

module.exports = (function() {
    Emailer.prototype.options = {};

    Emailer.prototype.data = {};

    Emailer.prototype.attachments = [];

    function Emailer(options, data) {
        this.options = options;
        this.data = data;
    }

    Emailer.prototype.send = function(callback) {
        var attachments, html, messageData, transport;
        html = this.getHtml(this.options.template, this.data);
        attachments = this.getAttachments(html);
        messageData = {
            to: this.options.to.email,
            from: this.options.from,
            subject: this.options.subject,
            html: html,
            generateTextFromHTML: true,
            attachments: attachments
        };
        transport = this.getTransport();
        return transport.sendMail(messageData, callback);
    };

    Emailer.prototype.getTransport = function() {
        return emailer.createTransport("SMTP", {
            host: 'mail.stategrowth.tas.gov.au'
            //service: "Gmail",
            //auth: {
            //  user: "InventisRemote@gmail.com",
            //  pass: "Inventis123"
            //}
        });
    }

    Emailer.prototype.getHtml = function(templateName, data) {
        var encoding, templateContent, templatePath, compiled;
        templatePath = "./views/emails/" + templateName + ".html";
        templateContent = fs.readFileSync(templatePath, encoding = "utf8");
        compiled = _.template(templateContent);
        return compiled(data);
    };

    Emailer.prototype.getAttachments = function(html) {
        var attachment, attachments, _i, _len, _ref;
        attachments = [];
        _ref = this.attachments;
        for(_i = 0, _len = _ref.length; _i < _len; _i++) {
            attachment = _ref[_i];
            if(html.search("cid:" + attachment.cid) > -1) {
                attachments.push(attachment);
            }
        }

        return attachments;
    };

    return Emailer;
})();