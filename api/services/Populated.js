var _ = require('lodash');

module.exports = {
	// Sails doesn't populate collections with ID's by default.
	// This will take a populated object, and for all collections, 
	// will convert arrays of objects into arrays of id's.
	flatten: function(populatedObj, model, req) {
        if(!model)
            return;
		var associations = model.associations;
		// Sails stores the populated associations in populatedObj.associations[prop].value
		// By stringifying it then parsing that, the correct fields are populated, and any toJSON() rules
		// set up for removing parts of the object (e.g. passwords) are also executed.
		var jsonString = JSON.stringify(populatedObj);
		populatedObj = JSON.parse(jsonString);
		for(var prop in populatedObj) {
			if(populatedObj.hasOwnProperty(prop)) {
				var association = _.find(associations, {alias:prop});
				if(association) {
					if(association.type === 'collection') {                                                                             
						for(var i = 0; i < populatedObj[prop].length; i++) {
							// Just in case it was already flattened
							if(populatedObj[prop][i] && populatedObj[prop][i].hasOwnProperty('id')) {
								populatedObj[prop][i] = populatedObj[prop][i].id;
							}
						}
					} else if(association.type === 'model') {
	                    if(populatedObj[prop] && populatedObj[prop].hasOwnProperty('id')) {
	                        populatedObj[prop] = populatedObj[prop].id;
	                    }
					} else {
						console.log("Unhandled association type encountered : " + JSON.stringify(association));
					}
				}
			}
		}
		return populatedObj;
	}
}