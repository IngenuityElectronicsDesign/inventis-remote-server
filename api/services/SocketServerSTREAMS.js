var net = require('net');
var Q = require('q');

var enableSTREAMSMessageCrcTest = false;

var passwordSeed = 0x43;				// change to setting as a randome num
var passwordSeedOffset = 0x22;
var passwordOffset = 0x0000;			// this seems to not work in the STREAMS Explorer page - so set it to zero

var debugCommsId = -1;					// 0 for all devices, -1 for none


// Helper Functions

function string_of_enum(enumToUse,value) 
{
  for (var k in enumToUse) if (enumToUse[k] == value) return k;
  return "Unknown Enum Value:" + value;
}

function HexStringFromByte(byteValue) {
	if ( byteValue == null) {
		console.log("HexStringFromByte - byteValue null")
		return "null";
	}
	if ( byteValue <= 0x0F ) {
		return '0' + byteValue.toString(16).toUpperCase();
	}
	return byteValue.toString(16).toUpperCase();
}

function HexStringFromWord(wordValue) {
	if ( wordValue == null) {
		console.log("HexStringFromWord - wordValue null")
		return "null";
	}
	if ( wordValue <= 0x000F ) {
		return '000' + wordValue.toString(16).toUpperCase();
	} else if ( wordValue <= 0x00FF ) {
		return '00' + wordValue.toString(16).toUpperCase();
	} else if ( wordValue <= 0x0FFF ) {
		return '0' + wordValue.toString(16).toUpperCase();
	}
	return wordValue.toString(16).toUpperCase();
}

function IntByteFromString(valueString,start) {
	if ( valueString.length-start >= 2 ) {
		return parseInt( "0x" + valueString.charAt(start) + valueString.charAt(start+1) );
	} else {
		console.log("IntByteFromString - string data too short");
		return 0;
	}
}

function IntWordFromString(valueString,start) {
	if ( valueString.length-start >= 4 ) {
		return parseInt( "0x" + valueString.charAt(start) + valueString.charAt(start+1) + valueString.charAt(start+2) + valueString.charAt(start+3) );
	} else {
		console.log("IntByteFromString - string data too short");
		return 0;
	}
}

function HexStringByteFromByteArray(data,start) {
	if ( data.length-start >= 2 ) {
		return String.fromCharCode(data[start]) + String.fromCharCode(data[start+1]);
	} else {
		console.log("HexStringByteFromByteArray - string data too short");
		return "xx";
	}
}

function HexStringByteFromString(valueString,start) {
	if ( valueString.length-start >= 2 ) {
		return valueString.charAt(start) + valueString.charAt(start+1);
	} else {
		console.log("HexStringByteFromString - string data too short");
		return "xx";
	}
}

function HexStringWordFromByteArray(data,start) {
	if ( data.length-start >= 4 ) {
		return String.fromCharCode(data[start]) + String.fromCharCode(data[start+1]) + String.fromCharCode(data[start+2]) + String.fromCharCode(data[start+3]);
	} else {
		console.log("HexStringWordFromByteArray - string data too short");
		return "xxxx";
	}
}

function HexStringWordFromString(valueString,start) {
	if ( valueString.length-start >= 4 ) {
		return valueString.charAt(start) + valueString.charAt(start+1) + valueString.charAt(start+2) + valueString.charAt(start+3);
	} else {
		console.log("HexStringWordFromString - string data too short");
		return "xxxx";
	}
}

// Checksum Calculation

function GenCrc16(data,nByte) {
	var Polynominal = 0x1021;
	var InitValue = 0x0;

	var i, j, index = 0;
	var CRC = InitValue;
	var Remainder, tmp, short_c;
	for (i = 0; i < nByte; i++)
	{
		//Debug("CRC CHECK:" + data[index].toString(16));
		short_c = (0x00ff & (data[index]) & 0xffff) & 0xffff;
		tmp = ((CRC >> 8) ^ short_c) & 0xffff;
		Remainder = (tmp << 8) & 0xffff;
		for (j = 0; j < 8; j++)
		{

			if ((Remainder & 0x8000) != 0)
			{
				Remainder = ((Remainder << 1) ^ Polynominal) & 0xffff;
			}
			else
			{
				Remainder = (Remainder << 1) & 0xffff;
			}
		}
		CRC = ((CRC << 8) ^ Remainder) & 0xffff;
		index++;
	}
	return CRC;
}

function CalculatePassword(seed) {
	var xordbits, count, returnValue;
	var bit6, bit8, bit9;
	returnValue = (seed + passwordSeedOffset)%256;
	for ( var count=0; count<16; count++ ) {
		bit6 = ( returnValue & 0x20 ) != 0x00;
		bit8 = ( returnValue & 0x80 ) != 0x00;
		bit9 = ( returnValue & 0x100 ) != 0x00;
		if ( bit6 ^ bit8 ^ bit9 ) {
			xordbits = 1;
		} else {
			xordbits = 0;
		}
		returnValue = returnValue << 1;
		returnValue = returnValue + xordbits;
	}
	returnValue = returnValue + passwordOffset;
	return returnValue & 0xFFFF;
}

// STREAMS Data Packet Control Characters

var ControlCharacters = Object.freeze({
	SOH : 0x01,
	STX : 0x02,
	ETX : 0x03,
	EOT : 0x04,
	ACK : 0x06,
	NAK : 0x15
})

// Streams ApplicationMessage MI Codes

var MICodes = Object.freeze({
	RejectMessage				: 0x00,
	Acknowledge					: 0x01,
	StartSession				: 0x02,
	PasswordSeed				: 0x03,
	Password 					: 0x04,
	HeartbeatPoll				: 0x05,
	SignStatusReply 			: 0x06,
	EndSession					: 0x07,
	SystemReset					: 0x08,
	UpdateTime					: 0x09,
	SignSetTextFrame			: 0x0A,
	SignSetGraphicsFrame		: 0x0B,
	SignSetMessage				: 0x0C,
	SignSetPlan					: 0x0D,
	SignDisplayFrame			: 0x0E,
	SignDisplayMessage			: 0x0F,
	EnablePlan					: 0x10,
	DisablePlan					: 0x11,
	RequestEnabledPlans			: 0x12,
	ReportEnabledPlans			: 0x13,
	SignSetDimmingLevel			: 0x14,
	PowerONOFF					: 0x15,
	DisableEnableDevice			: 0x16,
	SignRequestStoredFMP		: 0x17,
	RetrieveFaultLog			: 0x18,
	FaultLogReply				: 0x19,
	ResetFaultLog				: 0x1A,
	SignExtendedStatusRequest	: 0x1B,
	SignExtendedStatusReply		: 0x1C,
	HARStatusReply				: 0x40
})

var ApplicationErrorCodes = Object.freeze({
	NoError						: 0x00,
	DeviceControllerOffLine		: 0x01,
	SyntaxErrorInCommand		: 0x02,
	LengthErrorInCommand		: 0x03,
	DataChecksumError			: 0x04,
	TextWithNonASCIICharacters	: 0x05,
	//	FrameTooLargeForSign		: 0x06,
	UnknownMICode				: 0x07,
	MICodeNotSupported			: 0x08,
	//	PowerIsOff					: 0x09,
	UndefinedDeviceNumber		: 0x0A,
	//	FontNotSupported			: 0x0B,
	//	ColourNotSupported			: 0x0C,
	//	OverlapsNotSupported		: 0x0D,
	//	DimmingLevelNotSupported	: 0x0E,
	//	FrameCurrentlyActive		: 0x0F,
	FacilitySwitchOverride		: 0x10,
	//	ConspicuityNotSupported		: 0x11,
	//	TransitionTimeNotSupported	: 0x12,
	//	FrameUndefined				: 0x13,
	//	PlanNotEnabled				: 0x14,
	//	PlanEnabled					: 0x15,
	//	SizeMismatch				: 0x16,
	//	FrameTooSmall				: 0x17,
	//	HARStrategyStoppedByMaster	: 0x18,
	//	HARVoiceOrStrategyUndefined	: 0x19,
	//	HARErrorInStrategyDefinition: 0x1A,
	//	HARVoiceDataError			: 0x1B,
	//	HARVoiceFormatNotSupported	: 0x1C,
	//	HARHardwareError			: 0x1D
})

var ControllerAndDeviceErrorCodes = Object.freeze({
	NoError						: 0x00,
	//	PowerFailure				: 0x01,
	CommsTimeoutError			: 0x02, // Communications timeout to AAWS or IRAD
	MemoryError					: 0x03, // Sd Card Error
	BatteryFailure				: 0x04, // Power failure in AAWS or IRAD
	InternalCommsFailure		: 0x05, // Lost contact with AAWS or IRAD
	SignLampFailure				: 0x06, // Lamp failure in AAWS or IRAD
	//	SignSingleLEDFailure		: 0x07,
	// 	more here we don't use so I have not included them...
	ControllerReset				: 0x0D, // Remote reset by STREAMS
	BatteryLow					: 0x0E, // Low battery voltage
	FacilitySwitchOverride		: 0x10, // Test swithc set to "Test"
	DoorSwitchTriggered			: 0x1C, // Door switch triggered
	DetectorFault				: 0x1D, // Detector Fault
})

// STREAMS Data Packet and Non Data Packet structures

function STREAMSDataPacket() {
	this.SOH 					= ControlCharacters.SOH
	this.NS 					= 0;
	this.NR						= 0;
	this.STX 					= ControlCharacters.STX;
	this.ADDR					= 0;
	this.ApplicationMessage 	= "00";
	this.CRC					= 0;	// not used in the packet - added to the buffer
	this.ETX 					= ControlCharacters.ETX;
}

function STREAMSNonDataPacket(command) {
	this.CMD 					= command;
	this.NR						= 0;
	this.ADDR					= 0;
	this.CRC					= 0;	// not used in the packet - added to the buffer
	this.ETX 					= ControlCharacters.ETX;
}

// Sign Status Reply Data and Message Structures abd Functions

function SignDataPacket(signId,signErrorCode,signEnabled,signOn) {
	this.SignId 			= signId + 1;
	this.SignErrorCode 		= signErrorCode;
	this.SignEnabled		= signEnabled;
	this.SignOn				= signOn;
	this.FrameRevision		= 0;
	this.MessageIdDisplayed	= 0;
	this.MessageRevision	= 0;
	this.PlanIdDisplayed	= 0;
	this.PlanRevision		= 0;
}

function SignDataMessage(signDataPacket) {
	var signDataMessage = "";
	signDataMessage+= HexStringFromByte(signDataPacket.SignId);
	signDataMessage+= HexStringFromByte(signDataPacket.SignErrorCode);
	signDataMessage+= signDataPacket.SignEnabled ? "01" : "00";
	signDataMessage+= signDataPacket.SignOn ? "01" : "00";
	signDataMessage+= HexStringFromByte(signDataPacket.FrameRevision);
	signDataMessage+= HexStringFromByte(signDataPacket.MessageIdDisplayed);
	signDataMessage+= HexStringFromByte(signDataPacket.MessageRevision);
	signDataMessage+= HexStringFromByte(signDataPacket.PlanIdDisplayed);
	signDataMessage+= HexStringFromByte(signDataPacket.PlanRevision);
	return signDataMessage;
}

function SignStatusReplyPacket(miCode,online,appErrorCode,dateTime,controllerChecksum,controllerErrorCode,signs) {
	var dateTimeObj = new Date(dateTime);	// We do this in case the dateTime comes in as a string from the db
	this.MICode 				= miCode;
	this.Online					= online;
	this.AppError				= appErrorCode;
	this.Day 					= dateTimeObj.getDate();
	this.Month					= dateTimeObj.getMonth() + 1;
	this.Year					= dateTimeObj.getFullYear();
	this.Hours					= dateTimeObj.getHours();
	this.Minutes				= dateTimeObj.getMinutes();
	this.Seconds				= dateTimeObj.getSeconds();
	this.ControllerChecksum		= controllerChecksum;
	this.ControllerErrorCode	= controllerErrorCode;
	if ( signs == null ) {
		this.NumberOfSigns			= 0;
		this.SignData				= null;
	} else {
		this.NumberOfSigns			= signs.length;
		this.SignData				= signs;
	}
}

function SignStatusReplyMessage(signStatusReplyPacket) {
	var applicationMessage = "";
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.MICode);
	applicationMessage+= signStatusReplyPacket.Online ? "01" : "00";
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.AppError);
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.Day);
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.Month);
	applicationMessage+= HexStringFromWord(signStatusReplyPacket.Year);
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.Hours);
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.Minutes);
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.Seconds);
	applicationMessage+= HexStringFromWord(signStatusReplyPacket.ControllerChecksum);
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.ControllerErrorCode);
	applicationMessage+= HexStringFromByte(signStatusReplyPacket.NumberOfSigns);
	if ( signStatusReplyPacket.NumberOfSigns != 0 ) {
		signStatusReplyPacket.SignData.forEach(function(sign) {
			applicationMessage+= SignDataMessage(sign);
		});
	}
	return applicationMessage;
}

// Sign Extended Status Reply Data and Message Strucutres and Functions

function SignExtendedDataPacket(signId,signType,signRows,signCols,signErrorCode,signDimMode,signLumLevel,signStatusData) {
	this.SignId 			= signId + 1;
	this.SignType 			= signType;		// text or graphics
	this.SignRows			= signRows;
	this.SignCols			= signCols;
	this.SignErrorCode 		= signErrorCode;
	this.SignDimMode		= signDimMode;	// automatic or manual
	this.SignLumLevel		= signLumLevel;
	this.SignStatusData 	= signStatusData;
}

function SignExtendedDataMessage(signExtendedDataPacket) {
	var signExtendedDataMessage = "";
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignId);
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignType);
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignRows);
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignCols);
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignErrorCode);
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignDimMode);
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignLumLevel);
	signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignStatusData.length);
	for ( var i=0; i<signExtendedDataPacket.SignStatusData.length; i++) {
		signExtendedDataMessage+= HexStringFromByte(signExtendedDataPacket.SignStatusData[i]);
	}
	return signExtendedDataMessage;
}

function SignExtendedStatusReplyPacket(miCode,online,appErrorCode,manCode,dateTime,controllerErrorCode,signs) {
	var dateTimeObj = new Date(dateTime);	// We do this in case the dateTime comes in as a string from the db
	this.MICode 				= miCode;
	this.Online					= online;
	this.AppError				= appErrorCode;
	this.ManufacturerCode		= manCode;
	this.Day 					= dateTimeObj.getDate();
	this.Month					= dateTimeObj.getMonth() + 1;
	this.Year					= dateTimeObj.getFullYear();
	this.Hours					= dateTimeObj.getHours();
	this.Minutes				= dateTimeObj.getMinutes();
	this.Seconds				= dateTimeObj.getSeconds();
	this.ControllerErrorCode	= controllerErrorCode;
	this.NumberOfSigns			= signs.length;
	this.SignData				= signs;
}

function SignExtendedStatusReplyMessage(signExtendedStatusReplyPacket) {
	var applicationMessage = "";
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.MICode);
	applicationMessage+= signExtendedStatusReplyPacket.Online ? "01" : "00";
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.AppError);
	for( var i=0 ;i<signExtendedStatusReplyPacket.ManufacturerCode.length; i++ ) {
		applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.ManufacturerCode[i]);
	}
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.Day);
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.Month);
	applicationMessage+= HexStringFromWord(signExtendedStatusReplyPacket.Year);
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.Hours);
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.Minutes);
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.Seconds);
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.ControllerErrorCode);
	applicationMessage+= HexStringFromByte(signExtendedStatusReplyPacket.NumberOfSigns);
	signExtendedStatusReplyPacket.SignData.forEach(function(sign) {
		applicationMessage+= SignExtendedDataMessage(sign);
	});

	// calculate the checksum for this applicationMessage (why? - because the protocol says so)
	var checksumCalculationBuffer = new Buffer(applicationMessage.length);
	var index = 0;
	for ( var j=0; j<applicationMessage.length; j++) {
		checksumCalculationBuffer[index++] = applicationMessage.charCodeAt(j);
	}
	var checksum = GenCrc16(checksumCalculationBuffer,index);
	var crcHex = HexStringFromWord(checksum);

	// add the checksum to the end of the applicationMessage
	applicationMessage+= crcHex;

	return applicationMessage;
}

// Fault Log Reply Data and Message Structures abd Functions

function FaultLogPacket(id,entryNumber,dateTime,errorCode,faultState,headerName) {
	var dateTimeObj = new Date(dateTime);	// We do this in case the dateTime comes in as a string from the db
	this.Id 				= id + 1;
	this.EntryNumber 		= entryNumber;
	this.Day 				= dateTimeObj.getDate();
	this.Month				= dateTimeObj.getMonth() + 1;
	this.Year				= dateTimeObj.getFullYear();
	this.Hours				= dateTimeObj.getHours();
	this.Minutes			= dateTimeObj.getMinutes();
	this.Seconds			= dateTimeObj.getSeconds();
	this.ErrorCode 			= errorCode;
	this.FaultState 		= faultState;
	this.HeaderName 		= headerName;
	this.Created 			= dateTimeObj;
}

function FaultLogMessage(faultLogPacket) {
	var faultLogMessage = "";
	//console.log("FaultLogMessage:" + JSON.stringify(faultLogPacket,null,'\t'));
	faultLogMessage+= HexStringFromByte(faultLogPacket.Id);
	faultLogMessage+= HexStringFromByte(faultLogPacket.EntryNumber);
	faultLogMessage+= HexStringFromByte(faultLogPacket.Day);
	faultLogMessage+= HexStringFromByte(faultLogPacket.Month);
	faultLogMessage+= HexStringFromWord(faultLogPacket.Year);
	faultLogMessage+= HexStringFromByte(faultLogPacket.Hours);
	faultLogMessage+= HexStringFromByte(faultLogPacket.Minutes);
	faultLogMessage+= HexStringFromByte(faultLogPacket.Seconds);
	faultLogMessage+= HexStringFromByte(faultLogPacket.ErrorCode);
	faultLogMessage+= HexStringFromByte(faultLogPacket.FaultState);
	return faultLogMessage;
}

function FaultLogReplyPacket(streamsFaultLogs) {
	this.MICode 				= MICodes.FaultLogReply;
	this.NumLogs				= streamsFaultLogs.length;
	this.StreamsFaultLogs		= streamsFaultLogs;
}

function FaultLogReplyMessage(faultLogReplyPacket) {
	var applicationMessage = "";
	//console.log("FaultLogReplyMessage:" + JSON.stringify(faultLogReplyPacket,null,'\t'));
	applicationMessage+= HexStringFromByte(faultLogReplyPacket.MICode);
	applicationMessage+= HexStringFromByte(faultLogReplyPacket.NumLogs);
	faultLogReplyPacket.StreamsFaultLogs.forEach(function(SFLog) {
		applicationMessage+= SFLog.streamsFaultLog;
	});
	return applicationMessage;
}

function ACKReplyPacket(acknowledgedMICode) {
	this.MICode 				= MICodes.Acknowledge;
	this.AcknowledgedMICode		= acknowledgedMICode;
}

function ACKReplyMessage(ackReplyPacket) {
	var applicationMessage = "";
	applicationMessage+= HexStringFromByte(ackReplyPacket.MICode);
	applicationMessage+= HexStringFromByte(ackReplyPacket.AcknowledgedMICode);
	return applicationMessage;
}

function RejectReplyPacket(rejectedMICode,applicationErrorCode) {
	this.MICode 				= MICodes.RejectMessage;
	this.RejectedMICode			= rejectedMICode;
	this.ApplicationErrorCode	= applicationErrorCode;
}

function RejectReplyMessage(rejectReplyPacket) {
	var applicationMessage = "";
	applicationMessage+= HexStringFromByte(rejectReplyPacket.MICode);
	applicationMessage+= HexStringFromByte(rejectReplyPacket.RejectedMICode);
	applicationMessage+= HexStringFromByte(rejectReplyPacket.ApplicationErrorCode);
	return applicationMessage;
}

function PasswordSeedReplyPacket() {
	this.MICode 				= MICodes.PasswordSeed;
	this.Seed					= 0x43;
}

function PasswordSeedReplyMessage(rejectReplyPacket) {
	var applicationMessage = "";
	applicationMessage+= HexStringFromByte(rejectReplyPacket.MICode);
	applicationMessage+= HexStringFromByte(rejectReplyPacket.Seed);
	return applicationMessage;
}


// Server Events and Functions
function STREAMSServerCode(c) {

	var commsId = "?";
	var packetsReceivedCounter = 0;
	var packetsSentCounter = 0;
	var sessionEstablished = false;
	var STREAMSMessageCrcTestCounter = 0;
	var faultLogEntryNumber = 0;

	// Debug and Helper Functions

	function Debug(debugOutput) {
		if ( debugCommsId == 0 || commsId == debugCommsId ) {
			console.log( "S[" + commsId + "] " + debugOutput );
		}
	}

	function ErrorDebug(debugOutput) {
		console.log( "S[" + commsId + "] " + debugOutput );
	}

	function GetMessageCode(applicationMessage) {
		return messageCode = parseInt( "0x" + applicationMessage[0] + applicationMessage[1] );
	}

	function DebugSignDataMessage(message,start) {
		var index = start;
		Debug("--DebugSignDataMessage ---");
		Debug("  SignId:      " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  SignErr:     " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  SignEnabled: " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  SignOn:      " + HexStringByteFromString(message,index) ); index+=2;
		Debug("--DebugSignDataMessage --- END");
	}

	function DebugSignStatusReplyMessage(message) {
		var index = 0;
		Debug("-DebugSignStatusReplyMessage ---");
		Debug(" MICode: " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug(" Online: " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" AppErr: " + HexStringByteFromString(message,index) );																			index+=2;
		Debug(" Date:   " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Month:  " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Year:   " + HexStringWordFromString(message,index) + " (" + IntWordFromString(message,index) + ")" );							index+=4;
		Debug(" Hours:  " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Mins:   " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Secs:   " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" ConCrc: " + HexStringWordFromString(message,index) + " (" + IntWordFromString(message,index) + ")" );							index+=4;
		Debug(" ConErr: " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;

		// don't add anything to the index from here until we grab the number of signs... 
		Debug(" NSigns: " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							
		var numSigns = IntByteFromString(message,index);
		// add to the index, now that we have calculated the number of signs... it's used as the base for adding the sign debugs...
		index+=2;
		Debug("------ l:" + message.length + " l-" + index + ":" + (message.length - index) ); 
		for( var i=0; i<numSigns; i++) {
			DebugSignDataMessage(message, index + i * (message.length-index) / numSigns);
		}
		Debug("-DebugSignStatusReplyMessage --- END");
	}

	function DebugSignExtendedDataMessage(message,start) {
		var index = start;
		Debug("--DebugSignExtendedDataMessage ---");
		Debug("  SignId:      " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  SignType:    " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  SignRows:    " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  SignCols:    " + HexStringByteFromString(message,index) ); index+=2;
		Debug("  SignErr:     " + HexStringByteFromString(message,index) ); index+=2;
		Debug("  SignDim:     " + HexStringByteFromString(message,index) ); index+=2;
		Debug("  SignLum:     " + HexStringByteFromString(message,index) ); index+=2;
		Debug("  SignStatLen: " + HexStringByteFromString(message,index) ); index+=2;
		Debug("  SignStat:    " + HexStringByteFromString(message,index) ); index+=2;
		Debug("--DebugSignExtendedDataMessage --- END");
	}

	function DebugSignExtendedStatusReplyMessage(message) {
		var index = 0;
		Debug("-DebugSignExtendedStatusReplyMessage ---");
		Debug(" MICode: " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug(" Online: " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" AppErr: " + HexStringByteFromString(message,index) );																			index+=2;
		Debug(" ManCode:" + HexStringWordFromString(message,index)          + HexStringWordFromString(message,index+4) +
							HexStringWordFromString(message,index+8)        + HexStringWordFromString(message,index+12) +
							HexStringWordFromString(message,index+16) ); 																		index+=20;
		Debug(" Date:   " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Month:  " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Year:   " + HexStringWordFromString(message,index) + " ("   + IntWordFromString(message,index) + ")" );							index+=4;
		Debug(" Hours:  " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Mins:   " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" Secs:   " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;
		Debug(" ConErr: " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							index+=2;

		// don't add anything to the index from here until we grab the number of signs... 
		Debug(" NSigns: " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );
		var numSigns = IntByteFromString(message,index);
		// add to the index, now that we have calculated the number of signs... it's used as the base for adding the sign debugs...
		index+=2;
		Debug("------ l:" + message.length + " l-4-" + index + ":" + (message.length - 4 - index) ); 
		for( var i=0; i<numSigns; i++) {
			DebugSignExtendedDataMessage(message, index + i * (message.length-4-index) / numSigns);
		}
		Debug(" AMsgCRC:" + HexStringWordFromString(message,message.length-4) );
		Debug("-DebugSignExtendedStatusReplyMessage --- END");
	}

	function DebugFaultLogMessage(message,start) {
		var index = start;
		Debug("--DebugFaultLogMessage ---");
		Debug("  Id:         " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  EntryNum:   " + HexStringByteFromString(message,index) );	index+=2;
		Debug("  Date:       " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );					index+=2;
		Debug("  Month:      " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );					index+=2;
		Debug("  Year:       " + HexStringWordFromString(message,index) + " ("   + IntWordFromString(message,index) + ")" );					index+=4;
		Debug("  Hours:      " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );					index+=2;
		Debug("  Mins:       " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );					index+=2;
		Debug("  Secs:       " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );					index+=2;
		Debug("  ErrCode:    " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );					index+=2;
		Debug("  FaultState: " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );					index+=2;
		Debug("--DebugFaultLogMessage --- END");
	}

	function DebugFaultLogReplyMessage(message) {
		var index = 0;
		Debug("-DebugFaultLogReplyMessage ---");
		Debug(" MICode: " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;

		// don't add anything to the index from here until we grab the number of signs... 
		Debug(" NLogs:  " + HexStringByteFromString(message,index) + "   (" + IntByteFromString(message,index) + ")" );							
		var numLogs = IntByteFromString(message,index);
		// add to the index, now that we have calculated the number of signs... it's used as the base for adding the sign debugs...
		index+=2;
		Debug("------ l:" + message.length + " l-" + index + ":" + (message.length - index) ); 
		for( var i=0; i<numLogs; i++) {
			DebugFaultLogMessage(message, index + i * (message.length-index) / numLogs);
		}
		Debug("-DebugFaultLogReplyMessage --- END");
	}

	function DebugACKReplyMessage(message) {
		var index = 0;
		Debug("-DebugACKReplyMessage ---");
		Debug(" MICode: " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug(" AcknowledgedMICode:  " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug("-DebugACKReplyMessage --- END");
	}

	function DebugRejectReplyMessage(message) {
		var index = 0;
		Debug("-DebugRejectReplyMessage ---");
		Debug(" MICode: " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug(" RejectedMICode:  " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug(" ApplciationErrorMICode:  " + HexStringByteFromString(message,index) + "   (" + string_of_enum(ApplicationErrorCodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug("-DebugRejectReplyMessage --- END");
	}

	function DebugPasswordSeedReplyMessage(message) {
		var index = 0;
		Debug("-DebugPasswordSeedReplyMessage ---");
		Debug(" MICode: " + HexStringByteFromString(message,index) + "   (" + string_of_enum(MICodes,IntByteFromString(message,index)) + ")" );	index+=2;
		Debug(" Seed:  " + HexStringByteFromString(message,index) );
		Debug("-DebugPasswordSeedReplyMessage --- END");
	}

	function DebugSTREAMSDataPacket(sDataPack) {
		Debug("DebugSTREAMSDataPacket ---");
		Debug("NS:    " + sDataPack.NS);
		Debug("NR:    " + sDataPack.NR);
		Debug("ADDR:  " + sDataPack.ADDR);
		Debug("AppMsg:" + sDataPack.ApplicationMessage);
		Debug("MICode:" + string_of_enum(MICodes,GetMessageCode(sDataPack.ApplicationMessage)));
		//Debug("CRC:   " + "Not Calculated Here - see DebugSTREAMSData Output");
		Debug("DebugSTREAMSDataPacket --- END");
	}

	function DebugSTREAMSNonDataPacket(sDataPack) {
		Debug("DebugSTREAMSNonDataPacket ---");
		Debug("CMD:   " + string_of_enum(ControlCharacters,sDataPack.CMD));
		Debug("NR:    " + sDataPack.NR);
		//Debug("ADDR:  " + sDataPack.ADDR);
		//Debug("CRC:   " + "Not Calculated Here - see DebugSTREAMSData Output");
		//Debug("DebugSTREAMSNonDataPacket --- END");
	}

	function DebugSTREAMSData(data) {
		if ( data == null) {
			Debug("DebugSTREAMSData --- null data");
			return;
		}
		Debug("DebugSTREAMSData --- Debug len=" + data.length );
    	var line = "";

    	/*
    	for (var i = 0; i < data.length; i++) {
        	var byte = data[i];
       		line += byte.toString(16) + " ";
    	}
    	Debug(line);
    	*/

    	for (var i = 0; i < data.length; i++) {
        	var byte = data[i];
        	if ( i == data.length - 5 ) {
        		line += " [";
        	} else if ( i == data.length - 1 ) {
        		line += "]";
        	}
        	if ( byte <= 15 ) {
        		line += " " + string_of_enum(ControlCharacters,byte) + " ";
        	} else if ( byte >= 48 && byte <= 57 ) {
	       		line += String.fromCharCode(byte);
        	} else if ( byte >= 65 && byte <= 70 ) {
	       		line += String.fromCharCode(byte);
        	} else if ( byte >= 97 && byte <= 102 ) {
	       		line += String.fromCharCode(byte);
        	} else {
        		line += "?" + byte.toString(16);
        	}
    	}
    	Debug(line);
		Debug("DebugSTREAMSData --- END");
	}

	function CheckSTREAMSDataChecksum(data) {
		if ( data == null) {
			ErrorDebug("CheckSTREAMSDataChecksum --- null data");
			return;
		}
		var len = data.length;
		if ( len < 8 ) {
			ErrorDebug("CRC Check - packet to short - len:" + len);
			return false;
		}
		var crcInt = GenCrc16( data, len-5 );
		var dataCrcString = "0x" + HexStringWordFromByteArray(data,len-5);
		var dataCrcInt = parseInt(dataCrcString);
		if ( dataCrcInt == crcInt) {
			//Debug("CRC Check OK");
			return true;
		} else {
			var crcHex = crcInt.toString(16); // the result is a string of 4 hex characters. This is the CRC of the packet
			ErrorDebug("CRC Check BAD - Crc:0x" + crcHex + " dataCrc:" + dataCrcString );
			return false;
		}
	}

	function CheckPassword(dataPack) {
		var messageCode = GetMessageCode(dataPack.ApplicationMessage);
		if ( messageCode == MICodes.Password ) {
			if ( IntWordFromString(HexStringWordFromString(dataPack.ApplicationMessage,2),0) != CalculatePassword(passwordSeed))
			Debug("Password:" + HexStringWordFromString(dataPack.ApplicationMessage,2) + " Calc:" + HexStringFromWord(CalculatePassword(passwordSeed)) );
			return true;
		}
		ErrorDebug("Error - CheckPassword not password Applicaiton Message:" + messageCode );
		return false;
	}

	function CreateMessageBuffer(dataPack) {
		var index = 0;
		var returnBuffer = new Buffer(4096);

		returnBuffer[index++] = ControlCharacters.SOH;
		returnBuffer[index++] = HexStringFromByte(dataPack.NS).charCodeAt(0);
		returnBuffer[index++] = HexStringFromByte(dataPack.NS).charCodeAt(1);
		returnBuffer[index++] = HexStringFromByte(dataPack.NR).charCodeAt(0);
		returnBuffer[index++] = HexStringFromByte(dataPack.NR).charCodeAt(1);
		returnBuffer[index++] = HexStringFromByte(dataPack.ADDR).charCodeAt(0);
		returnBuffer[index++] = HexStringFromByte(dataPack.ADDR).charCodeAt(1);
		returnBuffer[index++] = ControlCharacters.STX;

		var messageCode = GetMessageCode(dataPack.ApplicationMessage);

		switch (messageCode) {
			case MICodes.HeartbeatPoll:
			case MICodes.SignStatusReply:
			case MICodes.SignExtendedStatusRequest:
			case MICodes.SignExtendedStatusReply:
			case MICodes.FaultLogReply:
			case MICodes.RetrieveFaultLog:
			case MICodes.Acknowledge:
			case MICodes.RejectMessage:
			case MICodes.PasswordSeed:
				for ( var j=0; j<dataPack.ApplicationMessage.length; j++) {
					returnBuffer[index++] = dataPack.ApplicationMessage.charCodeAt(j);
				}
				break;
			case MICodes.RetrieveFaultLog:
				Debug("RFL");
				break;
			default:
				ErrorDebug("Error - CreateMessageBuffer - unsupported MICode:" + string_of_enum(MICodes,messageCode) ); 
				return;
		}


		var checksum = GenCrc16(returnBuffer,index);
		var crcHex = HexStringFromWord(checksum);

		returnBuffer[index++] = crcHex.charCodeAt(0);
		returnBuffer[index++] = crcHex.charCodeAt(1);
		returnBuffer[index++] = crcHex.charCodeAt(2);
		returnBuffer[index++] = crcHex.charCodeAt(3);

		returnBuffer[index++] = ControlCharacters.ETX;

		return returnBuffer.slice(0,index);
	}

	function CreateNonDataMessageBuffer(dataPack) {
		var index = 0;
		var returnBuffer = new Buffer(4096);

		returnBuffer[index++] = dataPack.CMD;
		returnBuffer[index++] = HexStringFromByte(dataPack.NR).charCodeAt(0);
		returnBuffer[index++] = HexStringFromByte(dataPack.NR).charCodeAt(1);
		returnBuffer[index++] = HexStringFromByte(dataPack.ADDR).charCodeAt(0);
		returnBuffer[index++] = HexStringFromByte(dataPack.ADDR).charCodeAt(1);

		var checksum = GenCrc16(returnBuffer,index);
		var crcHex = HexStringFromWord(checksum);

		returnBuffer[index++] = crcHex.charCodeAt(0);
		returnBuffer[index++] = crcHex.charCodeAt(1);
		returnBuffer[index++] = crcHex.charCodeAt(2);
		returnBuffer[index++] = crcHex.charCodeAt(3);

		returnBuffer[index++] = ControlCharacters.ETX;

		return returnBuffer.slice(0,index);
	}

	function GetSTREAMSErrorCodes(log) {
		if ( log == null ) {
			ErrorDebug("GetSTREAMSErrorCodes - log null");
			return null;
		}
		//Debug("GetSTREAMSErrorCodes FAULTS:" + JSON.stringify(log.fault,null,'\t') );
		var signErrorCodes = [];
		// determine signErrorCode from log status and fault entries - for the sign status reply, the last entry will come up first, so we order here in the reverse precendece we would like for the errors
		if ( log.status.RouterLost || !log.status.Active ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.InternalCommsFailure);
		}
		if ( log.fault.TestModeEnabled ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.FacilitySwitchOverride);
		}
		if ( log.fault.Lamp1Fault || log.fault.Lamp2Fault ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.SignLampFailure);
		}
		if ( log.fault.BatteryCritical ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.BatteryFailure);
		}
		if ( log.status.SdCardError ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.MemoryError);
		}
		if ( log.status.BatteryLow ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.BatteryLow);
		}
		if ( log.status.DoorOpen || log.fault.DoorOpen ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.DoorSwitchTriggered);
		}
		if ( log.fault.RadarTriggerError ) {
			signErrorCodes.push(ControllerAndDeviceErrorCodes.DetectorFault);
		}
		//Debug("GetSTREAMSErrorCodes STREAMS ERRORS" + JSON.stringify(signErrorCodes,null,'\t') );
		return signErrorCodes;
	}

	function SendSignStatusReply(addr,alertDevice) {

		Debug("   SendSignStatusReply for:" + alertDevice.deviceIdentifier + " ADDR:" + addr);
		var sDataPack = new STREAMSDataPacket();
		sDataPack.NS = packetsSentCounter;
		sDataPack.NR = packetsReceivedCounter;
		sDataPack.ADDR = addr;

		var numSigns = alertDevice.routers.length;

		//Debug("NumSigns:" + numSigns);

		if (numSigns==0) {
			//function SignStatusReplyPacket(miCode,online,appErrorCode,dateTime,controllerChecksum,controllerErrorCode,signs) {
			sDataPack.ApplicationMessage = SignStatusReplyMessage( new SignStatusReplyPacket(MICodes.SignStatusReply, 1, 0, new Date(), 0, 0, null) );
			var sendBuffer = CreateMessageBuffer(sDataPack);
			//DebugSTREAMSDataPacket(sDataPack);
			//DebugSTREAMSData(sendBuffer);
			// send sendBuffer
			c.write(sendBuffer,WriteCallback);
		} else {
			// fill the sign data
			var signs = [];
			alertDevice.routers.forEach(function(r) {
				//TODO: Should this include routerID == 0? As this is the controller device, perhaps it should not go in as a sign?
				// I guess I should check for the isSign value in the attachedRouterData...
				//Debug("R:" + r.routerId + " LL:" + r.latestLog.id );
				var signErrorCode = ControllerAndDeviceErrorCodes.NoError;
				var signErrorCodes = GetSTREAMSErrorCodes(r.latestLog);
				if ( signErrorCodes ) {
					if ( signErrorCodes.length>0 ) {
						// we just take the latest
						signErrorCode = signErrorCodes.pop();
						Debug("   SignErrorCode:" + signErrorCode);
					}
				}
				//function SignDataPacket(signId,signErrorCode,signEnabled,signOn) {
				signs.push(new SignDataPacket(r.latestLog.routerId,signErrorCode,r.latestLog.status.Active,r.latestLog.lampOn));
			});

			// find the router with Id == 0
			var index = _.findIndex(alertDevice.latestHeader.attachedRouterData, function(router) {
					return router.Id === 0;
			});
			if ( index <0 || index >= alertDevice.latestHeader.attachedRouterData.length) {
				ErrorDebug("SendSignStatusReply - Error index out of range:" + index );
				index = 0;
			}
			var online = alertDevice.latestHeader.attachedRouterData[index].Online;
			var appErrorCode = ApplicationErrorCodes.NoError;	// TODO: where does this come from?
			var controllerChecksum = 0x00;
			var controllerErrorCode = ControllerAndDeviceErrorCodes.NoError; // TODO: do we just get this from sign 0?

			// create the appication message
			//function SignStatusReplyPacket(miCode,online,appErrorCode,dateTime,controllerChecksum,controllerErrorCode,signs) {
			sDataPack.ApplicationMessage = SignStatusReplyMessage( new SignStatusReplyPacket(MICodes.SignStatusReply, online, appErrorCode, new Date(), controllerChecksum, controllerErrorCode, signs) );
			//DebugSignStatusReplyMessage( sDataPack.ApplicationMessage );
			var sendBuffer = CreateMessageBuffer(sDataPack);
			//DebugSTREAMSDataPacket(sDataPack);
			//DebugSTREAMSData(sendBuffer);
			// send sendBuffer
			c.write(sendBuffer,WriteCallback);
		}
	}

	function SendSignExtendedStatusReply(addr,alertDevice) {
		Debug("   SendSignExtendedStatusReply for:" + alertDevice.deviceIdentifier + " ADDR:" + addr);
		var sDataPack = new STREAMSDataPacket();
		sDataPack.NS = packetsSentCounter;
		sDataPack.NR = packetsReceivedCounter;
		sDataPack.ADDR = addr;

		var numSigns = alertDevice.routers.length;

		//Debug("NumSigns:" + numSigns);

		if (numSigns==0) {
			//function SignStatusReplyPacket(miCode,online,appErrorCode,dateTime,controllerChecksum,controllerErrorCode,signs) {
			sDataPack.ApplicationMessage = SignStatusReplyMessage( new SignStatusReplyPacket(MICodes.SignStatusReply, 1, 0, new Date(), 0, 0, null) );
			var sendBuffer = CreateMessageBuffer(sDataPack);
			//DebugSTREAMSDataPacket(sDataPack);
			//DebugSTREAMSData(sendBuffer);
			// send sendBuffer
			c.write(sendBuffer,WriteCallback);
		} else {
			// fill the sign data
			var signs = [];
			alertDevice.routers.forEach(function(r) {
				//TODO: Should this include routerID == 0? As this is the controller device, perhaps it should not go in as a sign?
				// I guess I should check for the isSign value in the attachedRouterData...
				//Debug("R:" + r.routerId + " LL:" + r.latestLog.id );
				var signType = 0x00;	// text or graphics
				var signRows = 1;		// we make a 1x1 sign in STREAMS Explorer
				var signCols = 1;		// we make a 1x1 sign in STREAMS Explorer
				var signErrorCode = ControllerAndDeviceErrorCodes.NoError;
				var signErrorCodes = GetSTREAMSErrorCodes(r.latestLog);
				if ( signErrorCodes ) {
					if ( signErrorCodes.length>0 ) {
						// we just take the latest
						signErrorCode = signErrorCodes.pop();
						Debug("   SignErrorCode:" + signErrorCode);
					}
				}
				var signDimMode = 0x00;					// n/a to us
				var signLumLevel = 0x00;				// n/a to us
				var signStatusData = new Buffer([0]); 	// bitwise fault indicator - we just put in zero indicating no faults in the non-existant leds!

				//function SignExtendedDataPacket(signId,signType,signRows,signCols,signErrorCode,signDimMode,signLumLevel,signStatusData) {
				signs.push(new SignExtendedDataPacket(r.latestLog.routerId,signType,signRows,signCols,signErrorCode,signDimMode,signLumLevel,signStatusData) );
			});

			// find the router with Id == 0
			var index = _.findIndex(alertDevice.latestHeader.attachedRouterData, function(router) {
					return router.Id === 0;
			});
			if ( index <0 || index >= alertDevice.latestHeader.attachedRouterData.length) {
				ErrorDebug("   SendExtendedSignStatusReply - Error index out of range:" + index );
				index = 0;
			}
			var online = alertDevice.latestHeader.attachedRouterData[index].Online;
			var appErrorCode = ApplicationErrorCodes.NoError;	// TODO: where does this come from?
			var manufacturerCode = new Buffer([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09]);
			var controllerChecksum = 0x00;
			var controllerErrorCode = ControllerAndDeviceErrorCodes.NoError; // TODO: do we just get this from sign 0?

			// create the appication message
			//function SignExtendedStatusReplyPacket(miCode,online,appErrorCode,manCode,dateTime,controllerErrorCode,signs) {
			sDataPack.ApplicationMessage = SignExtendedStatusReplyMessage( new SignExtendedStatusReplyPacket(MICodes.SignExtendedStatusReply, online, appErrorCode, manufacturerCode, new Date(), controllerErrorCode, signs) );
			//DebugSignExtendedStatusReplyMessage( sDataPack.ApplicationMessage );

			var sendBuffer = CreateMessageBuffer(sDataPack);

			//DebugSTREAMSDataPacket(sDataPack);
			//DebugSTREAMSData(sendBuffer);

			// send sendBuffer
			c.write(sendBuffer,WriteCallback);
		}

	}

	function SendFaultLogReply(addr,alertDevice) {
		Debug("   SendFaultLogReply for:" + alertDevice.deviceIdentifier + " ADDR:" + addr);
		var retrievedHeaderName = alertDevice.site + alertDevice.dest1 + alertDevice.dest2;
		var sDataPack = new STREAMSDataPacket();
		sDataPack.NS = packetsSentCounter;
		sDataPack.NR = packetsReceivedCounter;
		sDataPack.ADDR = addr;

		if ( alertDevice.faultLogEntryNumber ) {
			// we have a valid faultLogEntryNumber integer
			faultLogEntryNumber = alertDevice.faultLogEntryNumber;
			if ( faultLogEntryNumber != alertDevice.faultLogEntryNumber ) {
				Debug("   Set faultLogEntryNumber from alertDevice:" + faultLogEntryNumber );
			}
		}

		// fill the sign data
		Log.find({
			sentToStreams:false,
			headerName: retrievedHeaderName
		})
		.sort( 'created ASC')
		.exec(function findCB(err,found) {
			Debug("   Found " + found.length + " unsent fault logs");

			var previousLogPromises = [];

			while (found.length) {
				var foundLog = found.shift();
				if ( foundLog ) {
    				//Debug('---------- Found Log id:" + foundLog.id + " routerId:' + foundLog.routerId + "headerName:" + foundLog.headerName + " Date:" + foundLog.created);
    				var deferred = Q.defer();

					Log.findOne({
						routerId: foundLog.routerId,
						headerName: foundLog.headerName,
					  	created: { '<': new Date(foundLog.created) }		// assumption that we do NOT have simultaneous logs for a device
					})
					.sort( 'created DESC' )
					.exec(
						(function(foundLog, deferred) { 
							return function findCB(err,prevLogFound) {
								if( err ) {
									ErrorDebug("   Error finding prevous fault log : " + JSON.stringify(err));
									deferred.reject(err);
								} else if ( !prevLogFound ) {
									//Debug("   No prevLogFound for foundLog:" + foundLog.routerId );
									deferred.resolve( {foundLog:foundLog, prevLogFound:null} );
								} else {
									//Debug("   prevLogFound id:" + prevLogFound.id + " Date:" + prevLogFound.created + " for foundLog id:" + foundLog.id );
									deferred.resolve( {foundLog:foundLog, prevLogFound:prevLogFound} );
								}
							}
						})(foundLog, deferred));
					//Debug("Push previousLogPromises");
					previousLogPromises.push(deferred.promise);
				} else {
					ErrorDebug("   SendFaultLogReply - Error foundLog null");
					// ignore this and go through the rest of the foundLogs
				}
    		}

    		Q.allSettled(previousLogPromises)
    		.then(function(results) {

				var faultLogs = []; 		// max log entries is 20 - see limiting code below...
				var logEntry = 0;

			    //Debug("Results:" + results.length);

				results.forEach(function(result) {
					if ( result.state === "fulfilled" ) {
						// result.value is {foundLog,prevLogFound}
						if ( !result.value.foundLog ) {
							ErrorDebug("   Error processing foundLog null");
						} else {
							//Debug("1--------------------------------------");
							//Debug("FoundLog:" + JSON.stringify(result.value.foundLog,null,'\t'));
							//Debug("2--------------------------------------");
							//Debug("PreviLog:" + JSON.stringify(result.value.prevLogFound,null,'\t'));
							//Debug("3--------------------------------------");
							var signErrorCodes = GetSTREAMSErrorCodes(result.value.foundLog);
							var prevSignErrorCodes = GetSTREAMSErrorCodes(result.value.prevLogFound);
							if ( prevSignErrorCodes ) {
								//Debug("   Scanning " + prevSignErrorCodes.length + " Previous Error Sign Codes");
								while (prevSignErrorCodes.length) {
									// we go through all prev sign error codes and check to see if the error has been cleared
									var prevSignErrorCode = prevSignErrorCodes.pop();
									//Debug("   Pop: " + string_of_enum(ControllerAndDeviceErrorCodes,prevSignErrorCode) + " (" + prevSignErrorCode + ")");
									var index = _.findIndex(signErrorCodes, function(errorCode) {
  										return errorCode === prevSignErrorCode;
									});
									if ( index == -1 ) {
										//Debug("   Previous Sign Error Cleared");
										if ( logEntry < 20 ) {
											Debug("   Push Fault Log:" + logEntry + " --- prev err code:" + prevSignErrorCode + " state:cleared");
											faultLogs.push(new FaultLogPacket(result.value.foundLog.routerId,faultLogEntryNumber,result.value.foundLog.created, prevSignErrorCode, 0, result.value.foundLog.headerName));
											logEntry++;
											if ( faultLogEntryNumber < 255 ) {
												faultLogEntryNumber++;
											} else {
												faultLogEntryNumber=0;
											}
										}
									} else {
										//Debug("   Previous Sign Error Still exists");
									}
								}
							} else {
								//Debug("   No Previous Sign Error Codes");
							}
							if ( signErrorCodes ) {
								//Debug("   Scanning " + signErrorCodes.length + " Sign Error Codes");
								while (signErrorCodes.length) {
									var signErrorCode = signErrorCodes.pop();
									//Debug("   Pop: " + string_of_enum(ControllerAndDeviceErrorCodes,signErrorCode) + " (" + signErrorCode + ")");
									if ( logEntry < 20 ) {
										Debug("   Push Fault Log:" + logEntry + " --- sign err code:" + signErrorCode + " state:set");
										faultLogs.push(new FaultLogPacket(result.value.foundLog.routerId,faultLogEntryNumber,result.value.foundLog.created, signErrorCode, 1, result.value.foundLog.headerName));
										logEntry++;
											if ( faultLogEntryNumber < 255 ) {
												faultLogEntryNumber++;
											} else {
												faultLogEntryNumber=0;
											}
									}
								}
							} else {
								//Debug("   No Sign Error Codes");
							}

							// Thjis log has now been added to the reply, so we need to set the sentToStreams flag to true for this log 
							// find the Log
							Log.findOne(result.value.foundLog.id)
							.exec(function foundLogToUpdate(err, logToUpdate) {
								if(err) {
									ErrorDebug("   Error finding Log To Update: " + JSON.stringify(err));
								} else if(!logToUpdate) {
									ErrorDebug("   Error finding Log To Update logToUpdate null");
								} else {
									// Update the Log
									//Debug("Old sentToStreams : " + logToUpdate.sentToStreams + " : new sentToStreams : true" );
									Log.update(logToUpdate.id, {sentToStreams: true})
									.exec(function(err, updatedLogs) {
										if(err) {
											ErrorDebug("   Error updating Log : " + JSON.stringify(err));
										} else if(!updatedLogs) {
											ErrorDebug("   Error updating Log : No updated updatedLog object(s) passed back.");
										} else {
											//Done
											//Debug("   sentToStreams updated to TRUE for Log :" + updatedLogs[0].id );
										}
									});
								}
							});
						}
					} else {
						ErrorDebug("   Error processing finding prevous fault log");
					}
				});

				Debug( "   " + faultLogs.length + " Fault Logs Pushed");

				if ( !alertDevice.faultLogEntryNumber || (faultLogEntryNumber != alertDevice.faultLogEntryNumber) ) {
					// Write the faultLogEntryNumber back to the alertDevice
					AlertDevice.update(alertDevice.id, {faultLogEntryNumber: faultLogEntryNumber})
					.exec(function(err, updatedAlertDevices) {
						if(err) {
							ErrorDebug("   Error updating faultLogEntryNumber for alertDevicee : " + JSON.stringify(err));
						} else if(!updatedAlertDevices) {
							ErrorDebug("   Error updating faultLogEntryNumber for alertDevicee : No updated updated object(s) passed back.");
						} else {
							//Done
							Debug("   faultLogEntryNumber updated to " + updatedAlertDevices[0].faultLogEntryNumber + " for Log :" + updatedAlertDevices[0].id );
						}
					});
				}

				if ( faultLogs.length != 0 ) {

					//Debug("New fault logs created, wait until they are addedd to the database before sending");

					var faultLogPromises = [];

					// Add faultLogs to FaultLog model
					faultLogs.forEach(function(faultLog){
						var deferred = Q.defer();
						var newLog = {
							headerName: retrievedHeaderName,
							created: faultLog.Created,
							streamsFaultLog: FaultLogMessage(faultLog)
						}
						FaultLog.create( newLog )
						.exec( function(err, createdLog) {
							if( err ) {
								ErrorDebug("   Error creating streams fault log : " + JSON.stringify(err));
								deferred.reject(err);
							} else if ( !createdLog ) {
								ErrorDebug("   " + foundLog.routerId );
								deferred.reject("null created streams fault Log ");
							} else {
								//Debug("   prevLogFound id:" + prevLogFound.id + " Date:" + prevLogFound.created + " for foundLog id:" + foundLog.id );
								deferred.resolve(createdLog);
							}
						});
						faultLogPromises.push(deferred.promise);
					});

					// Retrieve last 20 logs
		    		Q.allSettled(faultLogPromises)
	    			.then(function(results) {
	  					Debug("   " + results.length + " new fault logs created");
						results.forEach(function(result) {
							if ( result.state !== "fulfilled" ) {
								ErrorDebug("    !! Error creating streams fault Logs");
		    				}
		    			});

		    			FaultLog.find({
							headerName: retrievedHeaderName
						})
						.limit(20)	
						.sort( 'created DESC')
						.exec(function findStreamsFaultLogs(err,found) {

							if( err ) {
								ErrorDebug("   Error finding streams fault logs : " + JSON.stringify(err));
							} else if ( !found ) {
								ErrorDebug("   Error streams fault logs null");
							} else {
								Debug("   Found " + found.length + " StreamsFaultLogs");

								// create the appication message
								sDataPack.ApplicationMessage = FaultLogReplyMessage( new FaultLogReplyPacket(found) );
								//DebugFaultLogReplyMessage( sDataPack.ApplicationMessage );

								var sendBuffer = CreateMessageBuffer(sDataPack);

								//DebugSTREAMSDataPacket(sDataPack);
								//DebugSTREAMSData(sendBuffer);

								// send sendBuffer
								c.write(sendBuffer,WriteCallback);
							}

						});

	    			});

				} else {

					// no new fault logs created, just check the old ones
					//Debug("    No new FaultLogs = just search through existing fault logs: " + retrievedHeaderName );
	    			FaultLog.find({
						headerName: retrievedHeaderName
					})
					.limit(20)	
					.sort( 'created DESC')
					.exec(function findStreamsFaultLogs(err,found) {

						if( err ) {
							ErrorDebug("   Error finding streams fault logs : " + JSON.stringify(err));
						} else if ( !found ) {
							ErrorDebug("   Error streams fault logs null");
						} else {
							Debug("   Found " + found.length + " StreamsFaultLogs");

							// create the appication message
							sDataPack.ApplicationMessage = FaultLogReplyMessage( new FaultLogReplyPacket(found) );
							//DebugFaultLogReplyMessage( sDataPack.ApplicationMessage );

							var sendBuffer = CreateMessageBuffer(sDataPack);

							//DebugSTREAMSDataPacket(sDataPack);
							//DebugSTREAMSData(sendBuffer);

							// send sendBuffer
							c.write(sendBuffer,WriteCallback);
						}

					});
				}

			});

 		});

	}

	function SendProtocolACKReply(addr) {
		Debug("   SendProtocolACKReply");
		var sDataPack = new STREAMSNonDataPacket();
		sDataPack.CMD = ControlCharacters.ACK;
		sDataPack.NR = packetsReceivedCounter;
		sDataPack.ADDR = addr;

		var sendBuffer = CreateNonDataMessageBuffer(sDataPack);

		//DebugSTREAMSNonDataPacket(sDataPack);
		//DebugSTREAMSData(sendBuffer);

		// send sendBuffer
		c.write(sendBuffer,WriteCallback);
	}

	function SendProtocolNAKReply(addr) {
		Debug("   SendProtocolNAKReply");
		var sDataPack = new STREAMSNonDataPacket();
		sDataPack.CMD = ControlCharacters.NAK;
		sDataPack.NR = packetsReceivedCounter;
		sDataPack.ADDR = addr;

		var sendBuffer = CreateNonDataMessageBuffer(sDataPack);

		//DebugSTREAMSNonDataPacket(sDataPack);
		//DebugSTREAMSData(sendBuffer);

		// send sendBuffer
		c.write(sendBuffer,WriteCallback);
	}

	function SendACKReply(addr,miCode) {
		Debug("   SendACKReply");
		var sDataPack = new STREAMSDataPacket();
		sDataPack.NS = packetsSentCounter;
		sDataPack.NR = packetsReceivedCounter;
		sDataPack.ADDR = addr;

		// create the appication message
		sDataPack.ApplicationMessage = ACKReplyMessage( new ACKReplyPacket(miCode) );
		//DebugACKReplyMessage( sDataPack.ApplicationMessage );

		var sendBuffer = CreateMessageBuffer(sDataPack);

		//DebugSTREAMSDataPacket(sDataPack);
		//DebugSTREAMSData(sendBuffer);

		// send sendBuffer
		c.write(sendBuffer,WriteCallback);
	}

	function SendRejectReply(addr,miCode) {
		Debug("   SendRejectReply");
		var sDataPack = new STREAMSDataPacket();
		sDataPack.NS = 0;
		sDataPack.NR = 0;
		sDataPack.ADDR = addr;

		// create the appication message
		sDataPack.ApplicationMessage = RejectReplyMessage( new RejectReplyPacket(miCode,ApplicationErrorCodes.DeviceControllerOffLine) );
		//DebugRejectReplyMessage( sDataPack.ApplicationMessage );

		var sendBuffer = CreateMessageBuffer(sDataPack);

		//DebugSTREAMSDataPacket(sDataPack);
		//DebugSTREAMSData(sendBuffer);

		// send sendBuffer
		c.write(sendBuffer,WriteCallback);
	}

	function SendPasswordSeedReply(addr) {
		Debug("   SendPasswordSeedReply");
		var sDataPack = new STREAMSDataPacket();
		sDataPack.NS = 0;
		sDataPack.NR = 0;
		sDataPack.ADDR = addr;

		// create the appication message
		sDataPack.ApplicationMessage = PasswordSeedReplyMessage( new PasswordSeedReplyPacket() );
		//DebugPasswordSeedReplyMessage( sDataPack.ApplicationMessage );

		var sendBuffer = CreateMessageBuffer(sDataPack);

		//DebugSTREAMSDataPacket(sDataPack);
		//DebugSTREAMSData(sendBuffer);

		// send sendBuffer
		c.write(sendBuffer,WriteCallback);
	}

	function FindAlertDevice(addr) {

		var deferred = Q.defer();

		AlertDevice.findOne(addr)
		.populate('routers')
	    .populate('latestHeader')
		.exec(function foundAlertDevice(err, alertDevice) {
			if( err ) {
				ErrorDebug("Error finding Alert Device : " + JSON.stringify(err));
				deferred.reject(new Error(err));
				return;
			}
			if( !alertDevice ) {
				ErrorDebug("No Device ID maching ADDR:" + addr + " found");
				deferred.reject(new Error("No Device ID maching ADDR:" + addr + " found"));
				return;
			}

			//Debug("Found Alert Device:" + alertDevice.deviceIdentifier + " with ID:" + addr );
			deferred.resolve(alertDevice);
		});

		return deferred.promise;
	}

	function AddNewLatestLog(r) {
        var deferred = Q.defer();
        Log.findOne(r.latestLog)
        .exec(function foundLatestLog(err, latestLog) {
			if( err ) {
				ErrorDebug("Error finding latestLog : " + JSON.stringify(err));
				deferred.reject(new Error(err));
				return;
			}
			if( !latestLog) {
				ErrorDebug("No latestLog found");
				deferred.reject(new Error("No latestLog found"));
				return;
			}

			//Debug("Found latestLog:" + latestLog.routerId );
			deferred.resolve(latestLog);
        });

        return deferred.promise;
    }


	// dump given character string as 8 bit values
	function ProcessSTREAMSData(data) {
		if ( data == null ) {
			ErrorDebug("Error: ProcessSTREAMSData - null data recieved")
		}
		if ( data.len < 8 ) {
			ErrorDebug("Error: ProcessSTREAMSData - data packet to short - len:" + data.len);
			return false;
		}
		if ( enableSTREAMSMessageCrcTest ) {
			if ( STREAMSMessageCrcTestCounter++ == 5 ) {
				// corrupt every 5th message
				Debug("STREAMS Message CRC Test - Corrupt data[5]");
				STREAMSMessageCrcTestCounter = 0;
				data[5] = 0x00;
			}
		}
		
		var len = data.length;
		switch ( data[0] ) {
			case ControlCharacters.SOH:
				//Debug("SOH found");
				if ( data[7] != ControlCharacters.STX ) {
					ErrorDebug("Error: ProcessSTREAMSData - STX in incorrect position");
					return;
				}
				if ( data[len-1] != ControlCharacters.ETX) {
					ErrorDebug("Error: ProcessSTREAMSData - no ETX at end of data");
					return;
				}
				var sDataPack 		= new STREAMSDataPacket();
				sDataPack.NS	= IntByteFromString(HexStringByteFromByteArray(data,1),0);
				sDataPack.NR	= IntByteFromString(HexStringByteFromByteArray(data,3),0); 
				sDataPack.ADDR	= IntByteFromString(HexStringByteFromByteArray(data,5),0); 
				sDataPack.CRC 	= HexStringWordFromByteArray(data,len-5);
				sDataPack.ApplicationMessage = "";
				for (var i=0; i<(len-5-8); i++ ) {
					sDataPack.ApplicationMessage += String.fromCharCode(data[8+i]);
				}

				if ( !CheckSTREAMSDataChecksum(data) ) {
					//DebugSTREAMSData(data);
					SendProtocolNAKReply(sDataPack.ADDR);
					return;
				}

				var messageCode = GetMessageCode(sDataPack.ApplicationMessage);
				// set the commsId once we decode the packet address ( we should never have multiple comms to the same address - if we do, this deebugging mechanism should be adjusted )
				commsId = sDataPack.ADDR;

				if ( sessionEstablished ) {
					packetsSentCounter = sDataPack.NS;
					if ( sDataPack.NS != sDataPack.NR || packetsReceivedCounter != sDataPack.NR ) {
						Debug("MICode:" + string_of_enum(MICodes,messageCode) );
						Debug("SEQ my R:" + packetsReceivedCounter + " pack R:" + sDataPack.NR );
						Debug("SEQ my S:" + packetsSentCounter + " pack S:" + sDataPack.NS );
						packetsReceivedCounter = sDataPack.NR;
						SendProtocolNAKReply(sDataPack.ADDR);
						return;
					}
					if ( packetsReceivedCounter < 255 ) {
						packetsReceivedCounter++;
					} else {
						packetsReceivedCounter=1;
					}

					FindAlertDevice(sDataPack.ADDR).then(function(alertDevice) {
						var latestLogPromises = [];
						alertDevice.routers.forEach(function(r) {
							latestLogPromises.push(AddNewLatestLog(r));
						});
						Q.allSettled(latestLogPromises)
						.then(function(results) {
							results.forEach(function(result) {
								if(result.state === "fulfilled") {
									// result array is [array of latestLog objects]
									var index = _.findIndex(alertDevice.routers, function(router) {
  										return router.latestLog === result.value.id;
									});
									//Debug("Result latestLog:" + alertDevice.routers[index].latestLog );
									alertDevice.routers[index].latestLog = result.value;
								} else {
									ErrorDebug("Error filling latestLogs");
									// TODO: log error
								}
							});

							switch (messageCode) {
								case MICodes.HeartbeatPoll:
									Debug("MICode: HeartbeatPoll");
									//DebugSTREAMSData(data);
									//DebugSTREAMSDataPacket(sDataPack);
									SendProtocolACKReply(sDataPack.ADDR);
									SendSignStatusReply(sDataPack.ADDR,alertDevice);
									break;
								case MICodes.RetrieveFaultLog:
									Debug("MICode: RetrieveFaultLog");
									//DebugSTREAMSData(data);
									//DebugSTREAMSDataPacket(sDataPack);
									SendProtocolACKReply(sDataPack.ADDR);
									SendFaultLogReply(sDataPack.ADDR,alertDevice);
									break;
								case MICodes.SignExtendedStatusRequest:
									Debug("MICode: SignExtendedStatusRequest");
									//DebugSTREAMSData(data);
									//DebugSTREAMSDataPacket(sDataPack);
									SendProtocolACKReply(sDataPack.ADDR);
									SendSignExtendedStatusReply(sDataPack.ADDR,alertDevice);
									break;
								case MICodes.SignDisplayFrame:
									Debug("MICode: SignDisplayFrame");
									//DebugSTREAMSData(data);
									//DebugSTREAMSDataPacket(sDataPack);
									SendProtocolACKReply(sDataPack.ADDR);
									SendACKReply(sDataPack.ADDR,messageCode);
									break;
								case MICodes.UpdateTime:
									Debug("MICode: UpdateTime");
									SendProtocolACKReply(sDataPack.ADDR);
									SendACKReply(sDataPack.ADDR,messageCode);
									break;
								case MICodes.StartSession:
									Debug("Start Session while sessionEstablished = true");
									//DebugSTREAMSData(data);
									//DebugSTREAMSDataPacket(sDataPack);
									sessionEstablished = false;
									SendProtocolACKReply(sDataPack.ADDR);
									SendRejectReply(sDataPack.ADDR,messageCode);
									break;
								case MICodes.EndSession:
									Debug("End Session");
									//DebugSTREAMSData(data);
									//DebugSTREAMSDataPacket(sDataPack);
									sessionEstablished = false;
									SendProtocolACKReply(sDataPack.ADDR);
									SendACKReply(sDataPack.ADDR,messageCode);
									break;
								default:
									ErrorDebug("Error - ProcessSTREAMSData - unsupported MICode:" + string_of_enum(MICodes,messageCode) ); 
									//SendProtocolNAKReply(sDataPack.ADDR);
									return;
							}

						});
					}, function(err) {
						ErrorDebug("Error - ADDR does not match any AlertDevice:" + sDataPack.ADDR ); 
						SendProtocolNAKReply(sDataPack.ADDR);
						return;
					});
				} else {
					Debug("No Session messageCode:" + messageCode );
					switch (messageCode) {
						case MICodes.StartSession:
							//DebugSTREAMSData(data);
							//DebugSTREAMSDataPacket(sDataPack);
							SendProtocolACKReply(sDataPack.ADDR);
							SendPasswordSeedReply(sDataPack.ADDR);
							break;
						case MICodes.Password:
							//DebugSTREAMSData(data);
							//DebugSTREAMSDataPacket(sDataPack);
							if ( CheckPassword(sDataPack) ) {
								sessionEstablished = true;
								SendProtocolACKReply(sDataPack.ADDR);
								SendACKReply(sDataPack.ADDR,messageCode);
							} else {
								// nak
								SendProtocolNAKReply(sDataPack.ADDR);
							}
							break;
						default:
							Debug("Session NOT Established - REJECT Message")
							//DebugSTREAMSData(data);
							//DebugSTREAMSDataPacket(sDataPack);
							SendProtocolACKReply(sDataPack.ADDR);
							SendRejectReply(sDataPack.ADDR,messageCode);
							break;
					}
				}
				break;
			case ControlCharacters.ACK:
				Debug("ACK found");
				break;
			case ControlCharacters.NAK:
				Debug("NAK found");
				break;
			default:
				ErrorDebug("Error - first byte unknown:" + data[0]);
				return;
		}
	}	

	var Send = function(command,parameter) {
		var sendString = command;
		if ( parameter != null ) {
			sendString+= ',' + parameter;
		}
		Debug("    Send: " + sendString);
		c.write(sendString,WriteCallback);
	}

	var WriteCallback = function() {
		//Debug("    WriteCallback");
	}

	// kick off the state machine
	Debug("Server Created");

	// setup all of the event handlers
	c.on('end', function() {
		Debug("Server disconnected");
	});

	c.on('connect', function() {
		Debug("New connection");
	});

	c.on('data', function(buffer) {
		ProcessSTREAMSData(buffer);
	});

	c.on('timeout', function() {
		Debug("Connection timeout");
	});

	c.on('drain', function() {
		Debug("Connection drain");
	});

	c.on('error', function(error){
		Debug("Connection error : " + JSON.stringify(error));
	});

	c.on('close', function(hadError) {
		Debug("Connection closed : " + hadError);
	})

}

// Spin up multiple servers

var STREAMSServers = {
	servers: [],
	initServers: function() {
		if(this.servers.length > 0)
			return;
		for ( var i=0; i<30; i++ ) {
			this.servers.push(net.createServer(STREAMSServerCode));
			this.servers[i].listen(1339+i);
			console.log("Init STREAMS Server on : " + (1339 + i));
		}
	}
};

module.exports = STREAMSServers;