/**
 * Bootstrap
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#documentation
 */

module.exports.bootstrap = function (cb) {

	// It's very important to trigger this callack method when you are finished 
 	// with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

  	User.find({}, function(err, users) {
  		if(err) {
  			console.log("Error finding initial user : " + JSON.stringify(err));
  			return cb();
  		}
  		if(!users) {
  			console.log("Error finding initial user : No Users object passed back.  Unexpected behaviour");
  			return cb();
  		}

  		if(users.length === 0) {
  			console.log("No Users in Database.  Generating default user....");
  			var defaultUser = {
  				email: 'its@dier.tas.gov.au',
  				password: 'dierpassword',
  				confirmation: 'dierpassword'
  			}

  			User.create(defaultUser, function(err, newUser) {
  				if(err) {
  					console.log("Error creating default User : " + JSON.stringify(newUser));
  					return cb();
  				}
  				console.log("Created default user : " + newUser.email);
  				cb();
  			});
  		} else {
        // Users exist so just cb
        cb();
      }
  	});

    console.log("Initializing STREAMS");
    SocketServerSTREAMS.initServers();
    console.log("Initializing ADC");
    SocketServerADC.initServer();
    console.log("DONE");
};